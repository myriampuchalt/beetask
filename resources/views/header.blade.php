<div class="brand-name navbar-header">
    <!-- Branding Image -->
    <a href="{{ url('/') }}"> {{ config('app.name', 'beeTask') }} </a>
</div>
<div class="header-menu collapse navbar-collapse navbar-menu" id="app-navbar-collapse">
@auth
    <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if(Auth::user()->hasRole('customer'))
                <li class="black-text">
                    <a href="{{ route('dashboard') }}">Post a Job</a>
                </li>
            @endif
            <li class="black-text">
                <a href="{{ route('profile') }}">My Account</a>
            </li>
            <li class="black-text dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                   aria-expanded="false"
                   aria-haspopup="true">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    @endauth
    @guest
        <a class="black-text" href="{{ route('login') }}">Login |</a>
        <!-- If we make these buttons, the login one could open the login modal-->
        {{--<a class="login">Login |</a>--}}
        <a class="black-text" href="{{ route('workerSignUp') }}">Become a BeeTasker</a>
    @endguest
</div>
