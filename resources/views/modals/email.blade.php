<!-- Modal -->
<div class="modal fade" id="customer-email" tabindex="-1" role="dialog" aria-labelledby="checkoutLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="email" role="form">
                <div class="modal-body">
                    <div id="email-form" class="container-fluid">
                        <label>Subject</label>
                        <input name="subject" class="form-control" type="text" value="beeWork Notification: Worker Delayed"><br>
                        <label>Content</label>
                        <textarea name="" class="form-control">We are really sorry to inform you that your worker will be delayed.</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="send">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->