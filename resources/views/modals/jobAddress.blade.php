<!-- Modal -->
<link href="{{ asset('css/account.css') }}?{{ time() }}" rel="stylesheet">
<div class="modal fade" id="jobAddress" tabindex="-1" role="dialog" aria-labelledby="dateAvailabilityLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="address"></div>
            </div>
        </div>
    </div>
</div>