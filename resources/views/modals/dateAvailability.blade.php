<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="dateAvailability" tabindex="-1" role="dialog" aria-labelledby="dateAvailabilityLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form id="form2">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p style="font-weight: bold">Daily Availability</p>
                <p readonly class="modal-title" id="myModalLabel" name="date"></p>
            </div>
            <div class="modal-body">
                <input type="checkbox" name="dailyAvailability[]" value="1" id="morning">
                <label for="">Morning</label>
                <input type="checkbox" name="dailyAvailability[]" value="2" id="afternoon">
                <label for="">Afternoon</label>
                <input type="checkbox" name="dailyAvailability[]" value="3" id="evening">
                <label for="">Evening</label>
                @if ($errors->has('checkbox'))
                    <span class="help-block">
                        <strong>{{ $errors->first('checkbox') }}</strong>
                    </span>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="saveDateAvailability" type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>