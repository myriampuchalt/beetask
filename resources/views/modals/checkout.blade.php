<!-- Modal -->
<div class="modal fade" id="checkout" tabindex="-1" role="dialog" aria-labelledby="checkoutLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Payment Details</h4>
            </div>
            <form id="bookNowform" action="" method="POST" role="form">
                <div class="modal-body">
                    <div id="payment-form" class="container-fluid">
                        <label>Billing Address</label>
                        <input name="firstLine" class="form-control" type="text" placeholder="First Line"><br>
                        <input name="secondLine" class="form-control" type="text" placeholder="Second Line"><br>
                        <input name="postcode" class="form-control" type="text" placeholder="Postcode"><br>
                        <input name="town" class="form-control" type="text" placeholder="Town"><br>
                        <label>Card Details</label>
                        <input name="name" class="form-control" type="text" placeholder="Name on Card"><br>
                        <div name="cardDetails" id="card-element" class="form-control"></div><br>
                        <div id="card-errors" role="alert" style="color: red;"></div><br>
                        <label>Total Price:</label> £<span id="totalAmount"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" id="bookNow" class="btn btn-primary">Book Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->