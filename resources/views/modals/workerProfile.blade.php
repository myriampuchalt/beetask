<!-- Modal -->
<div class="modal fade" id="workerProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <div class="workerProfileTitle"></div>
                </h4>
            </div>

            <div class="modal-body">
                <div class="workerProfile"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="selectWorker" type="button" class="btn btn-primary" data-dismiss="modal">@if(Auth::check()) Select @else Register @endif & Book</button>
            </div>

        </div>
    </div>
</div>
<!-- Modal -->
