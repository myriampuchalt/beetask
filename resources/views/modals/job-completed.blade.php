<!-- Modal -->
<div class="modal fade" id="job-completed" tabindex="-1" role="dialog" aria-labelledby="checkoutLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Mark Job As Completed</h4>
            </div>
            <form id="completed" action="" method="POST" role="form">
                <div class="modal-body">
                    <div id="job-info" class="container-fluid">
                        {{--<input name="job-id" class="form-control" type="text" value="12" disabled></label>--}}
                        <p>Are you sure you want to mark this job as completed?
                            <br>Submitting wrong information could compromise your rating.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="completed-with-issues" class="btn btn-danger">Completed with Issues</button>
                    <button type="button" id="completed" class="btn btn-primary">Completed</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->