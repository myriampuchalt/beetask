<!-- Modal -->
<div class="modal fade" id="cancel-job" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cancel Job</h4>
            </div>
            <form id="completed" action="" method="POST" role="form">
                <div class="modal-body">
                    <div id="job-info" class="container-fluid">
                        {{--<input name="job-id" class="form-control" type="text" value="12" disabled></label>--}}
                        <p>Are you sure you want to cancel this job?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" id="cancel" class="btn btn-primary">Yes, Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->