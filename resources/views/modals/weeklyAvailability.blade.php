<!-- Modal-->
<div class="modal fade" id="quickSelectAvailability" tabindex="-1" role="dialog" aria-labelledby="quickSelectAvailabilityLabel">
     <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="form1">
            <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title w-100 font-weight-bold" id="">Weekly Availability</h4>
            </div>
            <div class="modal-body">
                    <div class="row col-sm-12">
                        Morning: 08am - 13pm | Afternoon: 13pm - 18pm | Evening: 18pm +
                        <p></p>
                    </div>

                @foreach($availability as $dailyAvailability)
                    <div class="form-group">
                        <label class="col-sm-3">{{ date("l", mktime(0, 0, 0, 1, $dailyAvailability->day_of_week)) }}</label>
                        <div class="col-sm-9">
                            <input type="checkbox" name="{{$dailyAvailability->day_of_week}}[]" value="1" id="morning" @if($dailyAvailability->morning == 1) { checked } @endif>
                            <label>Morning </label>
                            <input type="checkbox" name="{{$dailyAvailability->day_of_week}}[]" value="2" id="afternoon" @if($dailyAvailability->afternoon == 1) { checked } @endif>
                            <label>Afternoon </label>
                            <input type="checkbox" name="{{$dailyAvailability->day_of_week}}[]" value="3" id="evening" @if($dailyAvailability->evening == 1) { checked } @endif>
                            <label>Evening </label>
                            @if ($errors->has('checkbox'))
                                <span class="help-block"><strong>{{ $errors->first('checkbox') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    @endforeach
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="saveWeeklyAvailability" type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                </div>

         </form>

     </div>
</div>
</div>

