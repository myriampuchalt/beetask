@extends('layouts.app')

@section('content')

    <div class="spinner"></div>
    <div class="orderDetailsContainer">
        <div align="center" class="details-header">
            <div class="background">Contact & Address Details</div>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <p><span class="bold">Name:</span> {{ $user['name'] }} </p>
                        <p><span class="bold">Email:</span> {{$user['email']}} </p>
                    </div>
                    <div class="col-md-6">
                        <p><span class="bold">Address:</span></p>
                        {!! $address !!}
                    </div>
                </div>
            </div>
        </div>
        <div align="center" class="details-header">
            <div class="background">Job & Worker Details</div>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <p><span class="bold">Start Time:</span> {{$job['job_start']}}</p>
                        <p><span class="bold">Total Hours:</span> {{$job['duration']}}</p>
                        <p><span class="bold">Short Description: </span>{{$job['description']}}</p>
                    </div>
                    <div class="col-md-6">
                        <p><span class="bold">Category:</span> {{$job['category_name']}}</p>
                        <p><span class="bold">Worker Name:</span> {{$worker['name']}}</p>
                        <p><span class="bold">Total Price: </span>£{{$job['total_amount']}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div align="center">
            <button align="center" id="openCheckout" type="button" class="btn btn-primary" data-toggle="modal"
                    data-total-amount="{{$job['total_amount']}}"
                    data-dismiss="modal">Continue to checkout
            </button>
        </div>
    </div>

    @include('modals.checkout')

@endsection

@section('pagescript')
    <script src="{{ asset('js/checkout.js') }}"></script>
@endsection


