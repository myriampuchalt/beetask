@extends('layouts.app')

@section('stylesheet')
    <link href="{{ asset('css/homepage.css') }}" media="all" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="spinner"></div>
    @guest
        <div class="welcome-msg">Get Started Now!</div>
        <div class="book-job">
            <a href="{{ route('dashboard') }}">Book A Job </a>
        </div>
    @endguest
@endsection

@section('footer')
    <div class="footer">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Carousel indicators -->
            <ol class="carousel-indicators">
                @foreach($categories as $category)
                    <li data-target="#myCarousel" data-slide-to="{{$category['id']}}"
                        @if($category['id'] === 1) class="active" @endif></li>
                @endforeach
            </ol>
            <!-- Wrapper for carousel items -->
            <div class="carousel-inner">
                @foreach($categories as $category)
                    <div class="item @if($category['id'] === 1) active @endif">
                        <img style="padding-top: 3rem;" src="{{ asset('images/' . $category['ident'] . '.png') }}"
                             height="200" width="200">
                    </div>
                @endforeach
            </div>
            <!-- Carousel controls -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
@endsection

@include('modals.login')

@section('pagescript')
    <script src="{{ asset('js/homepage.js') }}?{{ time() }}"></script>
@endsection
