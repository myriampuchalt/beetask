@component('mail::message')
# Your order is confirmed!

Order number: {{ $jobId }}

@component('mail::button', ['url' => ''])
You are a customer
@endcomponent

Thanks,<br>
Your {{ config('app.name') }} team
@endcomponent
