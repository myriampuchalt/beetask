@component('mail::message')
# You have been booked for a job! 

Job ID: {{ $jobId }}

@component('mail::button', ['url' => ''])
You are a worker
@endcomponent

Thanks,<br>
Your {{ config('app.name') }} team
@endcomponent