@if(count($jobs) > 0)
    <div class="container">
        @foreach($jobs as $job)
            <div class="row" style="margin-bottom: 1rem;">
                <div class="col-xs-12 col-sm-10">
                    <div class="hidden-xs col-sm-1">{{$job['id']}}</div>
                    <div class="hidden-xs col-sm-3">{{$job['label']}}</div>
                    <div class="col-xs-4 col-sm-2">{{$job['date']}}</div>
                    <div class="col-xs-2 col-sm-1">{{$job['start_time']}}</div>
                    <div class="col-xs-2 col-sm-1">{{$job['end_time']}}</div>
                    @if (Auth::user()->hasRole('worker'))
                        <div class="col-xs-2 col-sm-2">{{$job['user_name']}}</div>
                    @elseif (Auth::user()->hasRole('customer'))
                        <div class="col-xs-2 col-sm-2">{{$job['worker_name']}}</div>
                    @endif
                    <div class="col-xs-2 col-sm-1">
                        @if($job['status'] == 2) Active @elseif ($job['status'] == 3) Completed @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    @if($job['start_time_object'] < \Carbon\Carbon::now())
                        <button type="button" class="btn btn-success margin-bottom" data-toggle="modal"
                                data-target="#job-completed" data-dismiss="modal">Mark Job as Completed
                        </button><br>
                    @else
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#cancel-job"
                                data-dismiss="modal">Cancel the job
                        </button>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@else
    <div class="errorMessages center">
        You have not completed any jobs yet.
    </div>
@endif



