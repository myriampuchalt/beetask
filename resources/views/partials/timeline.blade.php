<div class="siteMessage @if(isset($siteMessage)) visible @endif @if(!$userWillBeLate) in-time @endif ">@if(isset($siteMessage)) {{$siteMessage}}@endif</div>

@if(count($jobs) > 0)
    <p style="text-align: center" class="date">{{$date}}</p>
    <article class="timeline-entry begin"></article>

    @for($i=0; $i<count($jobs); $i++)
        <li>
            <div class="timeline-badge"><i style="" class="glyphicon glyphicon-check"></i></div>
            <div class="timeline-panel @if($jobs[$i]['next_job'] == true) next-job @endif">
                <div class="timeline-heading">
                    <h4 class="timeline-title">{{$jobs[$i]->label}}</h4>
                    <p>
                        <small class="text-muted"><i class="glyphicon glyphicon-time"></i> {{$jobs[$i]->start_time}}
                            - {{$jobs[$i]->duration}}</small>
                    </p>
                </div>
                <div class="timeline-body">
                    <p>{{$jobs[$i]->description}}</p>
                </div>
            </div>
            <div class="buttons-container">
                @if($jobs[$i]->start_time_object < \Carbon\Carbon::now())
                    <button type="button" class="btn btn-success margin-bottom" data-toggle="modal"
                            data-target="#job-completed" data-dismiss="modal">Mark Job as Completed
                    </button>
                @elseif ($jobs[$i]->start_time_object > \Carbon\Carbon::now())
                    <button type="button" class="btn btn-default margin-bottom" data-toggle="modal"
                            data-target="#customer-email" data-dismiss="modal">Notify a delay
                    </button>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#cancel-job"
                            data-dismiss="modal">Cancel the job
                    </button>
                @endif
            </div>
        </li>

        @if ($jobDistances && $i < count($jobDistances))
            <li class="timeline-inverted">
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title"> {{$jobDistances[$i]['dist']}}<br>{{$jobDistances[$i]['time']}}</h4>
                    </div>
                </div>
            </li>
        @endif

    @endfor

    <article class="timeline-entry begin">
        <div class="timeline-entry-inner">
            <div class="timeline-icon"></div>
        </div>
    </article>

@else
    <div class="errorMessages center">You don't have any jobs scheduled for this date.</div>
@endif