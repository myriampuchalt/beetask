@extends('account')

@section('accountContent')

    <div class="container">
        <div class="row">
            <div class="col-md-2 col-lg-2">
                <form class="form-horizontal" method="POST" action="{{ route('image') }}" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div align="center">
                        <img alt="User Profile Picture" src="{{ asset('images/profile/' . $profilePicture) }}"
                             class="img-responsive" height="150" width="150"><br>
                        <input type="file" name="image" accept=".jpg, .jpeg, .png" style="width: 150px"><br>
                        <button type="submit" class="btn btn-primary">Upload</button>
                        @if($errors->any())
                            <br><span style="color: #ff0300;">{{$errors->first()}}</span>
                        @endif
                    </div>
                </form>
            </div>

            <div class="col-md-10 col-lg-10">
                <form id="profile">
                    <div style="display: flow-root">

                        <button style="float: right" type="button"
                                class="btn btn-primary editProfile col-sm-3 col-xs-12">Edit Profile
                        </button>
                        <button style="float: right; display: none" type="button"
                                class="btn btn-primary saveProfile col-sm-3 col-xs-12">Save
                        </button>
                        <h4 class="profile-header">Personal Information</h4>
                    </div>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 profile-div">Nickname</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input name="nickname"
                                                                               class="form-input editField"
                                                                               value="{{$user['name']}}" disabled="true"
                                                                               readonly></div>
                            <div class="col-xs-12 col-sm-2 profile-div">Name</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input name="name"
                                                                               class="editField form-input"
                                                                               value="{{$address['name']}}"
                                                                               disabled="true" readonly></div>
                            <div class="col-xs-12 col-sm-2 profile-div">Surname</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input name="surname"
                                                                               class="editField form-input"
                                                                               value="{{$address['surname']}}"
                                                                               disabled="true" readonly></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 profile-div">Email</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input class="full-width form-input"
                                                                               value="{{$user['email']}}"
                                                                               disabled="true" readonly></div>
                            <div class="col-xs-12 col-sm-2 profile-div">Phone Number</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input name="phone_number"
                                                                               class="editField form-input"
                                                                               value="{{$user['phone_number']}}"
                                                                               disabled="true" readonly></div>
                            <div class="col-xs-12 col-sm-2 profile-div">Mobile</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input name="mobile_number"
                                                                               class="editField form-input"
                                                                               value="{{$user['mobile_number']}}"
                                                                               disabled="true" readonly></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 profile-div">House nr</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input id="street_number"
                                                                               name="street_number"
                                                                               class="full-width editField form-input"
                                                                               value="{{$address['street_number']}}"
                                                                               disabled="true" readonly></div>
                            <div class="col-xs-12 col-sm-2 profile-div">Address Ln</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input id="route"
                                                                               name="address"
                                                                               class="full-width editField form-input"
                                                                               value="{{$address['address']}}"
                                                                               disabled="true" readonly></div>
                            <div class="col-xs-12 col-sm-2 profile-div">Town/City</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input id="postal_town"
                                                                               name="town"
                                                                               class="full-width editField form-input"
                                                                               @if ($address['town'])
                                                                               value="{{$address['town']}}"
                                                                               @elseif ($address['locality'])
                                                                               value="{{$address['locality']}}"
                                                                               @endif
                                                                               disabled="true" readonly></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 profile-div">Postcode</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input id="postal_code"
                                                                               name="postcode"
                                                                               class="full-width editField form-input"
                                                                               value="{{$address['postcode']}}"
                                                                               disabled="true" readonly></div>
                            <div class="col-xs-12 col-sm-2 profile-div">County</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input id="administrative_area_level_1"
                                                                               name="administrative_area_level_1"
                                                                               class="full-width editField form-input"
                                                                               value="{{$address['administrative_area_level_1']}}"
                                                                               disabled="true" readonly></div>
                            <div class="col-xs-12 col-sm-2 profile-div">Country</div>
                            <div class="col-xs-12 col-sm-2 profile-div"><input name="country"
                                                                               class="full-width editField form-input"
                                                                               name="address_line"
                                                                               value="{{$address['country']}}"></div>
                        </div>


                        @if(Auth::user()->hasRole('worker'))
                            <h4>Categories & Rates</h4>
                            <div class="row">
                                <div class="btn-group-toggle" data-toggle="buttons">
                                    @foreach($categories as $category)
                                        <div style="display: inline-flex;" class="col-xs-12 col-sm-4">
                                            <div class="profile-div account-buttons">
                                                <button class="categoryBtn btn btn-default @if(in_array($category['id'], $workerCategories)) active @endif button-disabled"
                                                        disabled>
                                                    <input class="form-input" type="checkbox" style="display: none;"
                                                           name="category[]" value="{{$category['id']}}" disabled/>
                                                    {{$category['name']}}
                                                </button>
                                            </div>
                                            <div class=" profile-div">
                                                <input name="rateCategoryID.{{$category['id']}}"
                                                       class="form-input rate-input"
                                                       value="@if(isset($workerRates[$category['id']])) {{ $workerRates[$category['id']] }} @endif"
                                                       disabled/> £/h
                                            </div>
                                        </div>

                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('accountPageScript')
    <script src="{{ asset('js/accountProfile.js') }}?{{ time() }}"></script>
@endsection