@extends('account')

@section('accountContent')

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10" >
                <div class="panel panel-info">
                    <div class="panel-body">
                        <form role="form">
                            <div class="row form-group">
                                <div class=" col-md-12 col-lg-12">
                                    <table class="table table-user-information">
                                        <!-- We need to use the google autofill address instead of letting the user manually type in the address -->
                                        <thead>
                                        <th>Date</th>
                                        <th>Score</th>
                                        <th>Category</th>
                                        <th>Reviewer</th>
                                        <th>Comment</th>
                                        </thead>
                                        <tbody>
                                        @foreach($reviews as $review)
                                            <tr>
                                                <td>{{$review['created_at']}}</td>
                                                <td>{{$review['rating']}}</td>
                                                <td>{{$review['category_name']}}</td>
                                                <td>{{$review['reviewer_name']}}</td>
                                                <td>{{$review['content']}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection