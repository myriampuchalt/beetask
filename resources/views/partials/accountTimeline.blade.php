@extends('account')

@section('accountStyleSheet')
    <link href="{{ asset('css/accountTimeline.css') }}?{{ time() }}" rel="stylesheet">
@endsection

@section('accountContent')

    <div class="container">
        <div class="row">
            <form class="col-xs-12 col-sm-3 col-sm-offset-4" id="selected-date-form" role="form">
                <div class='input-group date' id='datepicker'>
                    <input id="date" type='text' name="date" class="form-control" placeholder="Select a date"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </form>
        </div>
        <ul id="timeline" class="timeline"></ul>
    </div>
@endsection

@include('modals.email')
@include('modals.job-completed')
@include('modals.cancel-job')

@section('accountPageScript')
    <script src="{{ asset('js/accountTimeline.js') }}?{{ time() }}"></script>
@endsection