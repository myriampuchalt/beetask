<div id="workersList">
    @php
    $i = 0;
    @endphp
    @foreach ($workers as $user)
        @php
            $i++;
        @endphp
        <div class="col-sm-3 col-md-3" @if($i===5) style="float: right;" @endif>
            <div class="workerContainer">
                <img src="{{ asset('images/profile/'. $user->profile_picture) }}" class="profile-picture" width="150">
                @if($user->rating)  <img src="{{ asset('images/'. $user->rating . 'stars.jpg') }}">
                @else <img src="{{ asset('images/0stars.jpg') }}">
                @endif
                <div class="caption">
                    <h5 class="no-margin">{{ $user->name }}</h5>
                    <h5 class="no-margin">£{{ $user->rate}}/h</h5>
                    <button href="#" id="{{ $user->worker_id }}" class="select-worker btn btn-primary">Select</button>
                </div>
            </div>
        </div>
    @endforeach
    @if(count($workers) === 0)
        <div class="no-workers">There are no available workers for your search.</div>
        @endif
</div>
<div id="workersPagination">
    {{ $workers->appends(request()->except('page'))->links() }}
</div>
