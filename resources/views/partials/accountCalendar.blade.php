@section('accountStyleSheet')
    <link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">
@endsection

@extends('account')

@section('accountContent')

    <div class="container">
        <div class="row">
            <div id="calendar" class="calendar col-xs-12"></div>
        </div>
    </div>

    @include('modals.dateAvailability')
    @include('modals.weeklyAvailability')

@endsection

@section('accountPageScript')
    <script src="{{ asset('js/accountCalendar.js') }}?{{ time() }}"></script>
    <script src="{{ asset('js/fullcalendar.js') }}"></script>
    <script src="{{ asset('js/scheduler.js') }}"></script>
@endsection