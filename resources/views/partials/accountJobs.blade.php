@extends('account')

@section('accountContent')

    <div class="container">
        <div class="row filter">
            <div class="col-xs-12 col-sm-3 col-sm-offset-5">
                Order By:
                <label class="radio-inline">
                    <input type="radio" name="orderBy" value="start_time" checked="checked"> Date
                </label>
                <label class="radio-inline">
                    <input type="radio" name="orderBy" value="id"> Job ID
                </label>
            </div>
        </div>
        <div class="row" style="font-weight: bold">
            <div class="col-xs-12 col-sm-10">
                <div class="hidden-xs col-sm-1">ID</div>
                <div class="hidden-xs col-sm-3">Label</div>
                <div class="col-xs-4 col-sm-2">Date</div>
                <div class="col-xs-2 col-sm-1">Start Time</div>
                <div class="col-xs-2 col-sm-1">End Time</div>
                @if (Auth::user()->hasRole('worker'))
                    <div class="col-xs-2 col-sm-2">Customer</div>
                @elseif (Auth::user()->hasRole('customer'))
                    <div class="col-xs-2 col-sm-2">Worker</div>
                @endif
                <div class="col-xs-2 col-sm-1">Status</div>
            </div>
        </div>
        <div class="row">
            <hr class="col-xs-12">
        </div>
        <div id="jobsContainer" class="row">
            @if($jobs)
                @include('partials.jobsContainer', $jobs)
            @endif

        </div>
    </div>

    {{--@include('modals.jobAddress')--}}
    @include('modals.email')
    @include('modals.job-completed')
    @include('modals.cancel-job')

@endsection

@section('accountPageScript')
    <script src="{{ asset('js/accountJobs.js') }}?{{ time() }}"></script>
@endsection