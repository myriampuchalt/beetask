<div class="bold">
    <h4>A bit about myself:</h4>
    <p>{{ $worker->description }}</p>
</div>
<br>
@if (count($reviews) > 0)
    <h3>Reviews:</h3>
    @foreach($reviews as $review)
        <div>
            <h4 class="reviewer-name">{{ $review->reviewer_name }}</h4>
            <p class="review">{{ $review->content }}</p>
        </div>
    @endforeach
@else
    <div class="errorMessages">This worker hasn't been reviewed yet.</div>
@endif
<div class="reviewsPagination">
    {{ $reviews->appends(request()->except('page'))->links() }}
</div>
