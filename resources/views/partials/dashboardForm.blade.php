<form id="post-job-form" action="" method="POST" role="form">
    <div class="post-job-form">
        <div class="form-group row">
            <div class="col-xs-11 col-sm-10">
                <select id="category" name="category" class="form-control">
                    <option selected>What do you need help with?</option>
                    @foreach($categories as $category)
                        <option value="{{$category['id']}}">{{$category['name']}}</option>
                    @endforeach
                </select>
                <p id="categoryError" class="errorMessages"></p>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-11 col-sm-10">
                <input id="dashboardAutocomplete" placeholder="Enter your address" type="text" class="form-control">
                <p id="addressError" class="errorMessages"></p>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-11 col-sm-10">
                <textarea id="taskDescription" name="taskDescription" class="form-control" rows="3"
                          placeholder="Describe your task very shortly"></textarea>
                <p id="descriptionError" class="errorMessages"></p>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-11 col-sm-10">
                <div class='input-group date' id='datepicker'>
                    <input type='text' name="date" class="form-control" placeholder="Select a date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <p id="dateError" class="errorMessages"></p>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-11 col-sm-10">
                <div class='input-group time' id='timepicker'>
                    <input type='text' name="time" class="form-control" placeholder="Select a time"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>
                <p id="timeError" class="errorMessages"></p>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-11 col-sm-10">
                <select id="totalHours" name="totalHours" class="form-control">
                    <option selected>How many hours does the job require?</option>
                    @for ($i=1; $i<11; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
                <p id="hoursError" class="errorMessages"></p>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-11 col-sm-10">
                <a id="findWorkers" class="btn btn-primary">Find Workers</a>
            </div>
        </div>
    </div>
</form>