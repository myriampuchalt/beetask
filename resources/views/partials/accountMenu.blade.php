<ul class="nav nav-pills navbar-nav navbar-center account-menu">
    <li role="presentation" class="{{ Request::is('account/profile') ? 'active' : '' }}" >
        <a href="{{ route('profile') }}">
            Profile
        </a>
    </li>
    <li role="presentation" class="{{ Request::is('account/jobs') ? 'active' : '' }}" >
        <a href="{{ route('jobs') }}">
            Your Jobs
        </a>
    </li>
    @if(Auth::user()->hasRole('worker'))
        <li role="presentation" class="{{ Request::is('account/timeline') ? 'active' : '' }}">
            <a href="{{ route('accountTimeline') }}">
                Your timeline
            </a>
        </li>
        <li role="presentation" class="{{ Request::is('account/calendar') ? 'active' : '' }}">
            <a href="{{ route('calendar') }}">
                Calendar
            </a>
        </li>
    @endif
</ul>