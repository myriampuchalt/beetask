<link href="{{ asset('css/signUp.css') }}" rel="stylesheet">

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1" >
                <div>
                    <div>
                        <h2 align="center">Complete your profile before you can start earning money!</h2>

                        <form id="signUpWorkerData" class="form-horizontal" method="POST" action="{{ route('completeSignUp') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                        <div align="center">
                            <h3>1.- Upload a photo of yourself</h3>
                            <div>
                                <img alt="User Profile Picture" src="{{ asset('images/profile/icon.png') }}" class="img-circle img-responsive" height="150" width="150"><br>
                                <input type="file" id="profilePicture" name="image" accept=".jpg, .jpeg, .png" style="width: 150px; display:none;">
                                <a id="upload">Upload Picture</a>
                                    @if($errors->has('image'))
                                        <div class="errorMessages">{{$errors->first('image')}}</div>
                                    @endif
                            </div>
                        </div>

                        <div align="center">
                            <h3>2.- Select your skills and add your rates</h3>

                            <div class="errorMessages">
                                @if($errors->has('category'))
                                    <div>{{$errors->first('category')}}</div>
                                @endif
                                @if($errors->has('worker_rate.*'))
                                    <div>{{$errors->first('worker_rate.*')}}</div>
                                @endif
                            </div>

                            <div class="btn-group-toggle" data-toggle="buttons">
                                @foreach($categories as $category)
                                    <div style="display: block">
                                        <button style="margin: 5px;" class="btn btn-default categoryBtn">
                                            <input class="categoryID" style="display: none;" type="checkbox" name="category[]" value="{{$category['id']}}">
                                            {{$category['name']}}
                                        </button>
                                        <div style="display: none" name="rateCategoryID.{{$category['id']}}">
                                            Your Rate:
                                            <input style="
                                            width: 6rem;
                                            border: 0rem;
                                            background-color: inherit;
                                            border-bottom: 0.1rem solid #888484;
                                            margin-bottom: 1rem;"
                                            > £/h
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div align="center">
                            <h3>3.- Your Personal Information</h3>
                            <div class="errorMessages">
                                @if($errors->has('first_name'))
                                    <div>{{$errors->first('first_name')}}</div>
                                @endif
                                @if($errors->has('surname'))
                                    <div>{{$errors->first('surname')}}</div>
                                @endif
                                @if($errors->has('phone_number'))
                                    <div>{{$errors->first('phone_number')}}</div>
                                @endif
                                @if($errors->has('address_line'))
                                    <div>{{$errors->first('address_line')}}</div>
                                @endif
                                @if($errors->has('postcode'))
                                    <div>{{$errors->first('postcode')}}</div>
                                @endif
                                @if($errors->has('town'))
                                    <div>{{$errors->first('town')}}</div>
                                @endif
                            </div>

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2">*First Name</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="first_name" style="border-radius: 0.5rem; border: 0.1rem solid" value="{{$user['name']}}"></div>
                                    <div class="col-xs-12 col-sm-2">Company Name</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="company_name" value="{{ old('company_name') }}" style="border-radius: 0.5rem; border: 0.1rem solid"></div>
                                    <div class="col-xs-12 col-sm-2">*Address Line</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="address_line" value="{{ old('address_line') }}" style="border-radius: 0.5rem; border: 0.1rem solid"></div>

                                    <div class="col-xs-12 col-sm-2">*Surname</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="surname" value="{{ old('surname') }}" style="border-radius: 0.5rem; border: 0.1rem solid"></div>
                                    <div class="col-xs-12 col-sm-2">*Phone Number</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="phone_number" style="border-radius: 0.5rem; border: 0.1rem solid" value="{{$user['phone_number']}}"></div>
                                    <div class="col-xs-12 col-sm-2">*Postcode</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="postcode" style="border-radius: 0.5rem; border: 0.1rem solid" value="{{$address['postcode']}}"></div>

                                    <div class="col-xs-12 col-sm-2">*Email</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" style="border-radius: 0.5rem; border: 0.1rem solid" value="{{$user['email']}}" disabled></div>
                                    <div class="col-xs-12 col-sm-2">Mobile Number</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="mobile_number" value="{{ old('mobile_number') }}" style="border-radius: 0.5rem; border: 0.1rem solid" ></div>
                                    <div class="col-xs-12 col-sm-2">*Town</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="town" value="{{ old('town') }}" style="border-radius: 0.5rem; border: 0.1rem solid"></div>
                                </div>
                            </div>
                        </div>
                        <div align="center">
                            <h3>4.- Your Bank Details</h3>

                            <div class="errorMessages">
                                @if($errors->has('account_number'))
                                    <div>{{$errors->first('account_number')}}</div>
                                @endif
                                @if($errors->has('sort_code'))
                                    <div>{{$errors->first('sort_code')}}</div>
                                @endif
                            </div>

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2 col-sm-offset-2">Account Number</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="account_number" value="{{ old('account_number') }}" style="border-radius: 0.5rem; border: 0.1rem solid"></div>
                                    <div class="col-xs-12 col-sm-2">Sort Code</div>
                                    <div class="col-xs-12 col-sm-2"><input class="form-input" name="sort_code" value="{{ old('sort_code') }}" style="border-radius: 0.5rem; border: 0.1rem solid"></div>
                                </div>
                            </div>

                        </div>

                        <div align="center">
                            <h3>5.- A brief description about yourself</h3>
                                @if($errors->has('worker_description'))
                                    <div class="errorMessages">{{$errors->first('worker_description')}}</div>
                                @endif

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <textarea name="worker_description" style="border-radius: 0.5rem; width: 100%; height: 10rem">{{ old('worker_description') }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div align="center">
                            <button type="submit" id="completeSignUp" class="btn btn-primary">Complete Sign Up</button>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pagescript')
    <script src="{{ asset('js/signUp.js') }}?{{ time() }}"></script>
@endsection