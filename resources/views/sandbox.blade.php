<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <script src="{{ asset('js/jquery.js') }}"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'beeTask') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}?{{ time() }}" rel="stylesheet">
    <link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datetimepicker-build.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard.css') }}?{{ time() }}" rel="stylesheet">

</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Branding Image -->
                <div>
                    <!-- <img alt="User Pic" src="" height="40" width="40"> -->
                    <a class="navbar-brand" href="{{ url('/') }}"> {{ config('app.name', 'beeTask') }} </a>
                </div>

            </div>

            <div class="collapse navbar-collapse navbar-menu" id="app-navbar-collapse">

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @auth
                        @if(Auth::user()->hasRole('customer'))
                            <li>
                                <a href="{{ route('dashboard') }}">
                                    Post a Job
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('profile') }}">
                                    Account
                                </a>
                            </li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>

    <div id="errorMessage" class="siteMessage"></div>

    <div id="mainContainer" class="container">
        <div align="center" style="height: 4rem">
            <div style="background-color: #3097D1; color: white; font-weight: bold">Contact & Address Details</div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <p><span style="font-weight: bold">Name:</span> {{ $user['name'] }}</p>
                    <p><span style="font-weight: bold">Email:</span> {{$user['email']}}</p>
                </div>
                <div class="col-md-6">
                    <p style="font-weight: bold">{!! $address !!}</p>
                </div>
            </div>
        </div>
        <div align="center" style="height: 4rem">
            <div style="background-color: #3097D1; color: white; font-weight: bold">Job & Worker Details</div>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <p><span style="font-weight: bold">Category:</span> {{$job['category_name']}}</p>
                        <p><span style="font-weight: bold">Start Time:</span> {{$job['job_start']}}</p>
                        <p><span style="font-weight: bold">Total Hours:</span> {{$job['duration']}}</p>
                    </div>
                    <div class="col-md-6">
                        <p><span style="font-weight: bold">Worker Name:</span> {{$worker['name']}}</p>
                        <p><span style="font-weight: bold">Price: </span>£{{$worker['rate']}}/h</p>
                    </div>
                </div>
            </div>
        </div>
        <div align="center">
            <button align="center" id="openCheckout" type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#checkout"
                    data-dismiss="modal">Continue to checkout
            </button>
        </div>
    </div>
</div>
</body>
</html>
