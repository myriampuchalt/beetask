@extends('layouts.app')

@section('stylesheet')
    <link href="{{ asset('css/account.css') }}?{{ time() }}" rel="stylesheet">
    @yield('accountStyleSheet')
@endsection

@section('content')

    <div class="menu">
        @include('partials.accountMenu')
    </div>

    <div class="accountContent" class="card text-center">
        @yield('accountContent')
    </div>

@endsection

@section('pagescript')
    @if (Auth::check() && Auth::user()->hasRole('worker'))
        <script src="{{ asset('js/workerAccount.js') }}?{{ time() }}"></script>
        <script src="{{ asset('js/notify.js') }}?{{ time() }}"></script>
    @endif

    @yield('accountPageScript')
@endsection