<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <script src="{{ asset('js/jquery.js') }}"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'beeTask') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}?{{ time() }}" rel="stylesheet">
    <link href="{{ asset('css/header.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datetimepicker-build.css') }}" rel="stylesheet">

    @yield('stylesheet')

</head>

<body class="layout">
<div class="body">
    <div class="header">
        @include('header')
    </div>
    <div id="app">
        <div id="errorMessage" class="siteMessage"></div>

        <div id="mainContainer" class="container">
            @yield('content')
        </div>
    </div>

    @yield('footer')

    @routes

    <!-- Scripts -->
    <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxbr23WyZBAQSBrc5tqIRd7hhLcT6Otl0&libraries=places"></script>
    <script src="{{ asset('js/underscore.js') }}"></script>
    <script src="{{ asset('js/app.js') }}?{{ time() }}"></script>
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
    <!-- Laravel Javascript Validation -->
    <script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\createJobPostRequest', '#post-job-form') !!}

    @if (Auth::check())
        <script>
            var userId = {{ Auth::id()}};
        </script>
    @endif

    @yield('pagescript')

</body>
</html>
