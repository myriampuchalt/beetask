@section('stylesheet')
    <link href="{{ asset('css/dashboard.css') }}?{{ time() }}" rel="stylesheet">
    @yield('dashboardStyleSheet')
@endsection
@extends('layouts.app')

@section('content')

    <div id="menu">
        @include('partials.dashboardForm')
    </div>

    <div id="workersContainer" class="row">
        <img id="loadingGif" src="{{ asset('images/loading.gif') }}"/>
    </div>

    @include('modals.workerProfile')
    @include('modals.register');
    @include('modals.workerNotAllowedError');

@endsection

@section('pagescript')
    <script defer src="{{ asset('js/dashboard.js') }}?{{ time() }}"></script>
    <script src="https://js.stripe.com/v3/"></script>
    @yield('dashboardPageScript')
@endsection