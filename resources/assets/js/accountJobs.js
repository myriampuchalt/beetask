$(document).ready(function () {

    $('.viewAddress').click(function () {

        var addressId = this.id;

        $.get(route('address'), { id: addressId }, function (data) {
            $('#jobAddress').on('shown.bs.modal', function () {
                $('#address').empty();
                var modalBody = $('#address');
                $('<p/>').text(data.name).appendTo(modalBody);
                $('<p/>').text(data.address).appendTo(modalBody);
                $('<p/>').text(data.town).appendTo(modalBody);
                $('<p/>').text(data.locality).appendTo(modalBody);
                $('<p/>').text(data.postcode).appendTo(modalBody);
                $('<p/>').text(data.administrative_area_level_1).appendTo(modalBody);
                $('<p/>').text(data.country).appendTo(modalBody);
            });
        });
    });

    $('input[type=radio][name=orderBy]').change(function() {

        $.get(route('filteredJobs'), { orderBy : this.value}, function(data) {
            $("#jobsContainer").html(data);
        })
    });
})