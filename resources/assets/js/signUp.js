$(document).ready(function () {

    var profilePictureInput = $("#profilePicture");
    var uploadLink = $("#upload");

    uploadLink.click(function() {
        profilePictureInput.click();
    });

    profilePictureInput.change(function () {
        profilePictureInput.show();
        uploadLink.hide();
    });

    $(".categoryBtn").click(function () {
        var categoryID = $(this).find('input').val();

        var workerRateDiv = $('[name="rateCategoryID.' + categoryID + '"]');

        if ($(this).find('input:checkbox:checked').length > 0) {
            workerRateDiv.hide();
        } else {
            workerRateDiv.find('input').attr('name', 'worker_rate[' + categoryID +']');
            workerRateDiv.show();
        }
    });
});