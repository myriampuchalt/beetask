window.moment = require('moment');
window.moment.locale('en');

$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#menu").height($("#content").height());

    $('#calendar').fullCalendar({
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        defaultView: 'agendaWeek',
        events: '',
        themeSystem: 'bootstrap3',
        height: 450,
        aspectRatio: 0,
        minTime: "08:00:00",
        maxTime: "24:00:00",
        allDaySlot: false,
        slotDuration: "01:00:01",
        header: {
            left: 'quickSelect',
            center: 'title',
            right: 'month agendaWeek'
        },
        footer: {
            right: 'prev,next'
        },
        customButtons: {
            quickSelect: {
                text: 'Weekly Availability',
                click: function click() {
                    $('#quickSelectAvailability').modal('show');
                }
            }
        },
        //selectable: true,

        // The override of the weekly availability by day has been removed for now
        // to make the retrieval of the workers easier when a customer creates a job

        // dayClick: function dayClick(date) {
        //
        //     $('#dateAvailability').modal('show').on('shown.bs.modal', function () {
        //         $('.modal-title').html(date.format('DD-MM-YYYY'));
        //     });
        // },

        eventSources: [
            {
                // jobs - I need to make it work with route()
                url: 'https://dev.beework/account/calendar/events',
                type: 'GET',
                dataType:'json',
                error: function() {
                    alert('there was an error while fetching events!');
                },
                color: '#F2EB8C',
                textColor: 'black',

            },
            {
                // holidays - this doesn't work fow now

                // url: 'http://dev.beework/account/calendar/holidays',
                // type: 'GET',
                // dataType:'json',
                // error: function() {
                //     //alert('there was an error while fetching events!');
                // },
                // color: '#F95252',
                // textColor: 'black'
            },
            {
                // availability - I need to make it work with route()
                color: '#ff331f',
                textColor: 'black',
                events: function (start, end, timezone, callback) {
                    $.ajax({
                    url: 'https://dev.beework/account/calendar/availability',
                    type: 'GET',
                    dataType:'json'
                    , success: function (response) {
                        var events = [];
                        $(response).each(function () {
                            events.push({
                                title: $(this).attr('title'),
                                start: $(this).attr('start'),
                                end: $(this).attr('end'),
                                allDay : $(this).attr('allDay'),
                            });
                        });
                        callback(events);
                    }
                    });
                }
            }
        ]
    });

    // This has been commented out for now because the daily availability functionality has been removed
    // $("#saveDateAvailability").click(function () {
    //     var data = $(" form ").serializeArray();
    //     $.post( route('calendar') , data, function() {
    //         $('#calendar').fullCalendar( 'refetchEvents' );
    //     });
    // });

    $("#saveWeeklyAvailability").click(function () {
        var data = $(" form ").serializeArray();
        $.post(route('calendar'), data)
            .then(function() {
                $('#calendar').fullCalendar('refetchEvents');
            });
    });
});

