$(document).ready(function () {

    var today = new Date();
    $('#dashboardAutocomplete').prop('value','');

    $(function () {
        $('#datepicker').datetimepicker({
            format: 'DD-MM-YYYY',
            minDate: today
        });
        $('#timepicker').datetimepicker({
            format: 'HH:mm',
            enabledHours: ['8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'],
            stepping: 60
        });
    });

    google.maps.event.addDomListener(window, 'load', initDashboardAutocomplete);

    var selectedCategory, dashboardAutocomplete, page;

    window.jobId = null;
    window.workerId = null;

    function initDashboardAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical location types.
        dashboardAutocomplete = new google.maps.places.Autocomplete(document.getElementById('dashboardAutocomplete'), { types: ['geocode'] });
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                dashboardAutocomplete.setBounds(circle.getBounds());
            });
        }
    }

    $("#dashboardAutocomplete").focus(function () {
        initDashboardAutocomplete();
        geolocate();
    });

    $("#menu").height($("#content").height());

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#findWorkers").click(function () {
        findWorkers();
    });

    $(document).on('click', '#workersPagination ul li a', function (e) {

        e.preventDefault();
        $("#loadingGif").show();

        var url = $(this).attr('href');
        var queryStringParams = url.split('?')[1];
        var attributes = queryStringParams.split('&');
        var page = 1;

        for (var i = 0; i < attributes.length; i++) {
            var value = attributes[i].split('=');
            if (value[0] == 'page') {
                page = value[1];
            }
        }

        findWorkers(page);
    });

    function findWorkers(page) {

        // $.get(route('checkoutData'), { workerId: 2 }, function (data) {
        //     $("#mainContainer").html(data);
        // });

        $("#workersContainer").empty();
        $(".errorMessages").empty();
        var addressError = true;

        // Get the address details from the autocomplete object if there is any.
        if (dashboardAutocomplete && dashboardAutocomplete.getPlace()) {
            var addressArray = processAddressAutocomplete(dashboardAutocomplete.getPlace());
            addressError = false;
        }

        if (addressError) {
            $("#addressError").html('Please Enter an Address');
        }

        var jobDetails = $("#post-job-form").serializeArray();
        var jobDetailsErrors = haveJobDetailsErrors(jobDetails);

        if (addressError || jobDetailsErrors) {
            return;
        }

        if (!page) {
            // If it's the first time we load the workers, we first need to save the address details into the DB and create a job record with the information provided by the customer
            $.post(route('createJob'), { jobDetails: jobDetails, address: addressArray, page: page }, function (data) {
                jobId = data.jobId;
            });
        }

        $.get(route('workers'), { jobDetails: jobDetails, page: page }, function (data) {
            $("#workersContainer").html(data);
        });
    };

    function processAddressAutocomplete(addressEntered) {
        return {
            'id': addressEntered.id,
            'lat': addressEntered.geometry.location.lat(),
            'lng': addressEntered.geometry.location.lng(),
            'street_number': findComponent(addressEntered, 'street_number'),
            'administrative_area_level_1': findComponent(addressEntered, 'administrative_area_level_1'),
            'administrative_area_level_2': findComponent(addressEntered, 'administrative_area_level_2'),
            'administrative_area_level_3': findComponent(addressEntered, 'administrative_area_level_3'),
            'locality': findComponent(addressEntered, 'locality'),
            'postcode': findComponent(addressEntered, 'postal_code'),
            'town': findComponent(addressEntered, 'postal_town'),
            'address': findComponent(addressEntered, 'route'),
            'country': findComponent(addressEntered, 'country')
        };
    };

    function haveJobDetailsErrors(jobDetails) {

        var errors = false;

        // jobDetails = [0 -> category, 1 -> taskDescription, 2 -> date, 3 -> time, 4 -> totalHours]
        if (jobDetails[0]['value'] == 'What do you need help with?') {
            $("#categoryError").html('Please Select a Category');
            errors = true;
        }

        if (jobDetails[1]['value'] == "") {
            $("#descriptionError").html('Please Enter a Description');
            errors = true;
        }

        if (jobDetails[2]['value'] == "") {
            $("#dateError").html('Please Select a Date');
            errors = true;
        }

        if (jobDetails[3]['value'] == "") {
            $("#dateError").html('Please Select a Time');
            errors = true;
        }

        if (jobDetails[4]['value'] == 'How many hours does the job require?') {
            $("#hoursError").html('Please Enter the amount of hours needed');
            errors = true;
        }

        return errors;
    };

    $("#category").change(function () {
        $('#workersContainer').empty();
        $("#categoryError").empty();
        selectedCategory = $(this).val();
    });

    $(document).on('click', ".select-worker", function () {

        workerId = this.id;
        var page = 1;

        $.get(route('worker', { id: workerId }), function (data) {
            $('.workerProfileTitle').html(data.name);
        });

        $.get(route('workerReviews'), { workerId: workerId, category: selectedCategory, page: page }, function (data) {
            $('#workerProfile').modal('show').on('shown.bs.modal', function () {
                $(".workerProfile").html(data);
            });
        });
    });

    $(document).on('click', "#openCheckout", function () {
        var totalAmount = $(this).data('total-amount');
        $('#checkout').modal('show').on('shown.bs.modal', function () {
            $('#totalAmount').html(totalAmount);
        });
    });

    $(document).on('click', '.reviewsPagination ul li a', function (e) {

        e.preventDefault();
        $("#loadingGif").show();

        var url = $(this).attr('href');
        var queryStringParams = url.split('?')[1];
        var attributes = queryStringParams.split('&');

        for (var i = 0; i < attributes.length; i++) {
            var value = attributes[i].split('=');
            if (value[0] == 'page') {
                page = value[1];
            }
        }

        $.get(route('workerReviews'), { workerId: workerId, category: selectedCategory, page: page }, function (data) {
            $(".workerProfile").empty();
            $(".workerProfile").html(data);
        });
    });

    $("#selectWorker").click(function () {
        $.get(route('userRole'), function (data) {
            if (data == 'false') {
                $('#register').modal('show');
            } else if (data == 'worker') {
                $('#workerNotAllowedError').modal('show');
            } else {
                $.get(route('checkoutData'), { workerId: workerId, jobId: jobId }, function (data) {
                    $(".layout").empty();
                    $(".layout").html(data);
                });
            }
        });
    });

    $("#continueBooking").click(function () {

        var registrationDetails = $(" form ").serializeArray();

        $.post(route('userRegister'), registrationDetails).then(function () {
            $.post(route('allocateUser'), { jobId: jobId });
        }).then(function () {
            $.get(route('checkoutData'), { workerId: workerId, jobId : jobId }, function (data) {
                $(".layout").empty();
                $(".layout").html(data);
            });
        }).fail(function (err) {
            var errors = err.responseJSON.errors;
            jQuery.each(errors, function (index, value) {
                $("#" + index + "").html(value);
            });
        });
    });

    function findComponent(result, type) {
        var component = _.find(result.address_components, function (component) {
            return _.includes(component.types, type);
        });
        return component && component.short_name;
    }
});