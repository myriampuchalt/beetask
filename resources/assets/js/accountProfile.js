$(document).ready(function () {

    // Autocomplete disabled for now

    // google.maps.event.addDomListener(window, 'load', initProfileAutocomplete);
    //
    // var profileAutocomplete;
    //
    // var componentForm = {
    //     route: 'long_name',
    //     street_number: 'short_name',
    //     postal_town: 'long_name',
    //     administrative_area_level_1: 'short_name',
    //     country: 'long_name',
    //     postal_code: 'short_name'
    // };
    //
    // function initProfileAutocomplete() {
    //     // Create the autocomplete object, restricting the search to geographical location types.
    //     profileAutocomplete = new google.maps.places.Autocomplete(document.getElementById('profileAutocomplete'), {types: ['geocode']});
    //     // While editing the user profile, when an address is selected from the dropdown, populate the address fields in the form.
    //     google.maps.event.addListener(profileAutocomplete, 'place_changed', fillInAddress);
    // }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    // getCurrentPosition() and watchPosition() no longer work on insecure origins.
    // To use this feature, we should consider switching the application to a secure origin, such as HTTPS.

    // function geolocate() {
    //     if (navigator.geolocation) {
    //         navigator.geolocation.getCurrentPosition(function (position) {
    //             var geolocation = {
    //                 lat: position.coords.latitude,
    //                 lng: position.coords.longitude
    //             };
    //             var circle = new google.maps.Circle({
    //                 center: geolocation,
    //                 radius: position.coords.accuracy
    //             });
    //             profileAutocomplete.setBounds(circle.getBounds());
    //         });
    //     }
    // }
    //
    // $("#profileAutocomplete").focus(function() {
    //     initProfileAutocomplete();
    //     geolocate();
    // });
    //
    // function fillInAddress() {
    //
    //     // First set the value of each address component to empty
    //     for (var component in componentForm) {
    //         document.getElementById(component).value = '';
    //         document.getElementById(component).disabled = false;
    //     }
    //
    //     // Get the place details from the autocomplete object if an adress has been entered.
    //     var place = profileAutocomplete.getPlace();
    //
    //     // Get each component of the address from the place details
    //     // and fill the corresponding field on the form.
    //     for (var i = 0; i < place.address_components.length; i++) {
    //         var addressType = place.address_components[i].types[0];
    //         if (addressType == 'locality') {
    //             addressType = 'town';
    //         }
    //         if (componentForm[addressType]) {
    //             var val = place.address_components[i][componentForm[addressType]];
    //             document.getElementById(addressType).value = val;
    //         }
    //     }
    // }

    $("#menu").height($("#content").height());

    $('.saveProfile').click(function () {

        // if (profileAutocomplete.getPlace()) {
        //     saveAddress();
        // }

        var data = $("#profile").serializeArray();
        $.post(route('profile'), data);

        $('.editField').prop('readonly', true);
        $('.rate-input').prop('disabled', true);
        $('.editField').prop('disabled', true);
        $('.addressField').prop('disabled', true);
        $('.saveProfile').hide();
        $('.editProfile').show();
        $('button.categoryBtn').prop('disabled', true);
        $('.categoryBtn').find('input').prop('disabled', true);
        $('.categoryBtn').addClass("button-disabled");
    });

    // function saveAddress() {
    //
    //     var place = profileAutocomplete.getPlace();
    //
    //     var placeArray = {
    //         'id': place.id,
    //         'lat': place.geometry.location.lat(),
    //         'lng': place.geometry.location.lng(),
    //         'street_number': findComponent(place, 'street_number'),
    //         'administrative_area_level_1': findComponent(place, 'administrative_area_level_1'),
    //         'administrative_area_level_2': findComponent(place, 'administrative_area_level_2'),
    //         'administrative_area_level_3': findComponent(place, 'administrative_area_level_3'),
    //         'locality': findComponent(place, 'locality'),
    //         'postcode': findComponent(place, 'postal_code'),
    //         'town': findComponent(place, 'postal_town'),
    //         'address': findComponent(place, 'route'),
    //         'country': findComponent(place, 'country')
    //     };
    //
    //     $.post(route('primaryAddress'), placeArray);
    // }
    //
    // function findComponent(result, type) {
    //     var component = _.find(result.address_components, function (component) {
    //         return _.includes(component.types, type);
    //     });
    //     return component && component.short_name;
    // }

    $('.editProfile').click(function () {
        if ($('.editField').is('[readonly]')) {
            $('.editField').prop('readonly', false);
            $('.editProfile').hide();
            $('.saveProfile').show();

            $('button.categoryBtn').prop('disabled', false);
            $('.profile-div').find('input').prop('disabled', false);
            $('.profile-div').removeClass("button-disabled");
        }

        if ($('.editField').is('[disabled]')) {
            $('.editField').prop('disabled', false);
        }
        if ($('.rate-input').is('[disabled]')) {
            $('.rate-input').prop('disabled', false);
        }
    });

    $('.addAddressManually').click(function () {

        if($('.addressField').is('[disabled]')) {
            $('.addressField').prop('disabled', false);
        }
    });
})