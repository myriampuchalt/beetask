$(document).ready(function () {

    var errorMessage = document.getElementById("errorMessage");
    var today = new Date();

    $('#datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        minDate: today
    }).on('dp.change', function () {
        getLocation().then(function(position) {
            getTimelineContent(position)
        }, function(err) {
            getTimelineContent();
        });
    });

    $('#datepicker').data("DateTimePicker").date(today);

    function getTimelineContent(position = null) {

        var date = $("#selected-date-form").serializeArray();
        date = date[0]['value'];

        $.get(route('timeline'), { date: date, position: position }, function (data) {
            $("#timeline").html(data);
        });
    };

    function getLocation() {

        return new Promise(function (resolve, reject) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    resolve(position);
                }, function(err) {
                    if (err.message == 'User denied Geolocation') {
                        errorMessage.innerHTML = 'Geolocation has been denied on your browser. Please allow it to check your current status';
                        $('#errorMessage').show();
                    }
                    reject(Error(err));
                });
            } else {
                errorMessage.innerHTML = "Geolocation is not supported by this browser.";
                $('#errorMessage').show();
            }
        });
    }
});