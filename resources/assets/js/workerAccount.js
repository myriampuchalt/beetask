$(document).ready(function () {

    $.notify.defaults({
        // default class (string or [string])
        className: "info",
        // whether to hide the notification on click
        clickToHide: true,
        // if autoHide, hide after milliseconds
        autoHideDelay: 5000,
        // default positions
        globalPosition: 'bottom right',
    });

    Echo.private('worker.' + window.userId).listen('JobAssigned', function (e) {
        // Add what you want to do with the event
        // $.notify.addStyle('job-assigned', {
        //     html:
        //         "<div class='info'>" +
        //         "<span class='info' data-notify-html='label'/><br>" +
        //         "<span class='info' data-notify-html='start_date'/> at " +
        //         "<span class='info' data-notify-html='start_time'/>.<br>" +
        //         "Total Duration: <span class='info' data-notify-html='duration'/> hours" +
        //         "</div>"
        // });

        // $.notify({
        //     label: e.label,
        //     start_date: e.start_date,
        //     start_time: e.start_time,
        //     duration: e.duration
        // }, {
        //     style: 'job-assigned',
        //     className: "info"
        // });

        $.notify(e.label + ' on the ' + e.start_date + ' at ' + e.start_time + '. Duration: ' + e.duration + ' hours.');
    });
});