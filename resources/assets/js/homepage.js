$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajaxSetup({cache: true});

    $.getScript('https://connect.facebook.net/en_GB/sdk.js', function () {
        FB.init({
            appId: '288913304990919',
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse social plugins on this page
            version: 'v3.1'
        });

        FB.getLoginStatus(function (response) {
            processFacebookStatus(response);
        });
    });

    function processFacebookStatus(response) {
        // If the user is logged into fbk with the browser but not into the app,
        // check on our db if the user has ever logged into the app with fbk
        if (response.status === 'connected' && !window.userId) {
            var facebookId = response.authResponse.userID;

            $.get(route('hasFacebookId', facebookId), function (response) {
                if (response == 'true') {
                    // if that is the case, log the user in automatically
                    $.post(route('loginFacebookUser'), {facebookId: facebookId}, function (response) {
                        if (response == 'true') {
                            window.location.replace("https://dev.beework/account/profile");
                        }
                    });
                } else {
                    $('#facebookContainer').hide();
                }
                ;
            });
        }
        ;
    };

    $(document).on('click', '.login', function () {
        $('#login').modal('show');
    });

    $(document).on('click', '#login', function (e) {
        e.preventDefault();
        var loginDetails = $(" form ").serializeArray();

        $.post("/login", loginDetails, function (data) {
        })
            .done(function () {
            })
            .fail(function (data) {
                //$('#login').modal('show');
            });
    });
});