$(document).ready(function () {

    var stripe = Stripe('pk_test_kGe7JGm760lJpzB8stxVQSwb');

    var style = {
        base: {
            color: '#303238',
            fontSize: '16px',
            color: "#32325d",
            fontSmoothing: 'antialiased',
            '::placeholder': {
                color: '#ccc',
            },
        },
        invalid: {
            color: '#e5424d',
            ':focus': {
                color: '#303238',
            },
        },
    };

    var elements = stripe.elements({
        fonts: [
            {
                cssSrc: 'https://fonts.googleapis.com/css?family=Roboto',
            }
        ],
        style: style
        // Stripe's examples are localized to specific languages, but if
        // you wish to have Elements automatically detect your user's locale,
        // use `locale: 'auto'` instead.
    });

    // Create an instance of the card Element.
    var card = elements.create('card', {iconStyle: 'solid', hidePostalCode: true});

    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    // Add an instance of the card Element into the `card-element` <div>.
    $(document).on('click', "#openCheckout", function () {
        $('#checkout').modal('show').on('shown.bs.modal', function () {
            if ($("#card-element")) {
                card.mount('#card-element');
            }
        });
    });

    // Create a token or display an error when the form is submitted.
    $(document).on('click', "#bookNow", function (e) {
        e.preventDefault();
        stripe.createToken(card)
            .then(function(result) {
            if (result.error) {
                // Inform the customer that there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
                $("#checkout").modal( "hide" );
                $(".orderDetailsContainer").hide();
                $(".spinner").show();
            }
        });
    });

    function stripeTokenHandler(token) {
        // Send the token ID to the server to submit the payment
        var billingDetails = $(" form ").serialize();
        var billingAmount = $("#totalAmount").text();
        $.post(route('bookJob'), { billingDetails: billingDetails, billingAmount: billingAmount, jobId: window.jobId, workerId: window.workerId, stripeToken: token.id }, function(data) {
            $(".spinner").hide();
            $("#mainContainer").html(data);
        });
    }
});