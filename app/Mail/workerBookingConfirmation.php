<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Job;

class WorkerBookingConfirmation extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $jobId;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($jobId)
    {
        $this->jobId = $jobId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.workerBookingConfirmation')
            ->with([
                'jobId' => $this->jobId
            ]);
    }
}
