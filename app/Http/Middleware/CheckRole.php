<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
	    if (! $request->user()->hasRole($role)) {
		    // Redirect...
		    // 404 Page Not Found: You are not allowed
		    // TO DO
	    }
        return $next($request);
    }
}
