<?php
/**
 * Created by PhpStorm.
 * User: Myriam
 * Date: 18/03/2018
 * Time: 18:22
 */

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection as Collection;
use App\WorkerAvailability;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Routing\Controller;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * @param Request $request
	 * @return string
	 */
    public function retrieveCalendarEvents(Request $request) :string
    {
        $userId = Auth::id();
        $jobs = DB::table('job')
            ->where('worker_id', $userId)
            ->whereDate('start_time', '>=', new Carbon($request->input('start')))
            ->whereDate('end_time', '<=', new Carbon($request->input('end')))
            ->get();

        $events = [];
        foreach ($jobs as $index => $jobInfo) {
            $events[] = [
                'title' => $jobInfo->label,
                'start' => $jobInfo->start_time,
                'end' => $jobInfo->end_time,
                'allDay' => false
            ];
        }
        return json_encode($events);
    }

    public function loadCalendarView()
    {
	    $availability = $this->retrieveWorkerWeeklyAvailability();
	    return view('partials.accountCalendar', ['availability' => $availability]);
    }

	/**
	 * @param Request $request
	 * @return string
	 */
    public function retrieveCalendarHolidays(Request $request) :string
    {
        $userId = Auth::id();

        $holidays = DB::table('worker_holidays')
            ->where('user_id', $userId)
            ->whereDate('start_date', '>=', new Carbon($request->input('start')))
            ->whereDate('end_date', '<=', new Carbon($request->input('end')))
            ->get();

        $events = [];

        foreach ($holidays as $index => $holidaysInfo) {

            $events[] = [
                'title' => 'HOLIDAYS',
                'start' => $holidaysInfo->start_date,
                'end' => $holidaysInfo->end_date,
                'allDay' => false
            ];
        }

        return json_encode($events);
    }

	/**
	 * By default, workers are unavailable all the time.
	 * If they want to make themselves available they need to do so on the Calendar.
	 * @return string
	 */
    public function retrieveWorkerAvailabilityForCalendar() : string
    {
        // Retrieve Weekly Availability for now
	    // When the daily override gets built we will retrieve the daily availability which overrides the weekly one

	    $availability = $this->retrieveWorkerWeeklyAvailability();
        $events = [];

	    // first we set when the week starts/ends and get the date of the first day of the week
	    // the days_of_week are saved in the same way in our DB
	    // Sunday -> 0, Saturday -> 6

	    Carbon::setWeekStartsAt(Carbon::SUNDAY);
	    Carbon::setWeekEndsAt(Carbon::SATURDAY);

	    $firstDayOfWeekDate = Carbon::now()->startOfWeek();

        foreach ($availability as $index => $availabilityInfo) {

	        $date = $firstDayOfWeekDate->copy()->addDays($availabilityInfo->day_of_week);

	        $morningStart = new Carbon($date->format('Y/m/d') . '08:00:00');
	        $afternoonStart = new Carbon($date->format('Y/m/d') . '13:00:00');
	        $eveningStart = new Carbon($date->format('Y/m/d') . '18:00:00');

	        if ($availabilityInfo->morning === 0) {
                $events[] = [
                    'title' => 'Unavailable',
                    'start' => $morningStart->toDateTimeString(),
                    'end' => $morningStart->addHours(5)->toDateTimeString(),
                    'allDay' => false
                ];
            }

            if ($availabilityInfo->afternoon === 0) {
                $events[] = [
                    'title' => 'Unavailable',
                    'start' => $afternoonStart->toDateTimeString(),
                    'end' => $afternoonStart->addHours(5)->toDateTimeString(),
                    'allDay' => false
                ];
            }

            if ($availabilityInfo->evening === 0) {
                $events[] = [
                    'title' => 'Unavailable',
                    'start' => $eveningStart->toDateTimeString(),
                    'end' => $eveningStart->addHours(6)->toDateTimeString(),
                    'allDay' => false
                ];
            }

        }

        return json_encode($events);
    }

    public function retrieveWorkerWeeklyAvailability()
    {
	    $userId = Auth::id();

	    $availability = DB::table('worker_availability')
		    ->where('user_id', $userId)
		    ->whereNull('date')
		    ->get();

	    // This needs to be fixed ASAP - we need to generate the array
	    if ($availability->count() === 0) {
            $availability = DB::table('worker_availability')
                ->where('user_id', 1)
                ->whereNull('date')
                ->get();
        }

	    return $availability;
    }

    // We update the availability for each day of the week or selected date
    private function updateAvailability($dayOfWeek, $availability, $date = null)
    {
        $userId = Auth::id();

        if ($date) {
            $workerAvailability = WorkerAvailability::findByDate($userId, $date);
        } else {
            $workerAvailability = WorkerAvailability::findByDayOfWeek($userId, $dayOfWeek);
        }

        if (!$workerAvailability) {
            $workerAvailability = new WorkerAvailability();
            $workerAvailability->user_id = $userId;
        }

        $workerAvailability->date = $date;
        $workerAvailability->day_of_week = $dayOfWeek;
        $workerAvailability->morning = false;
        $workerAvailability->afternoon = false;
        $workerAvailability->evening = false;

        foreach ($availability as $key => $value) {
            if ($value == 1) {
                $workerAvailability->morning = true;
            }

            if ($value == 2) {
                $workerAvailability->afternoon = true;
            }

            if ($value == 3) {
                $workerAvailability->evening = true;
            }
        }

        $workerAvailability->save();
    }

    // We process the form request and update the worker's availability accordingly
    public function processAvailability(Request $request)
    {
        $weeklyAvailability = [
            0 => [],
            1 => [],
            2 => [],
            3 => [],
            4 => [],
            5 => [],
            6 => []
        ];

        $date = null;

        foreach ($request->except('_token') as $key => $value) {

            if (is_int($key)) {
                $weeklyAvailability[$key] = $value;
            }

            // This isn't being used for now because we have disabled the daily availability override
            if ($key === 'date' && $value) {
                $date = new DateTime($value, new DateTimeZone('Europe/London'));
                $dailyAvailability = $request->input('dailyAvailability') ? $request->input('dailyAvailability') : [];
                $this->updateAvailability(null, $dailyAvailability, $date);
            }
        }

        foreach ($weeklyAvailability as $dayOfWeek => $availability) {
            $this->updateAvailability($dayOfWeek, $availability);
        }
    }

}