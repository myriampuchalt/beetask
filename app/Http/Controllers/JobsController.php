<?php

namespace App\Http\Controllers;

use App\Mail\BookingConfirmation;


class JobsController extends Controller
{
    public function sendBookingConfirmationEmail(Request $request, $jobId)
    {
        Mail::to($request->user())->send(new BookingConfirmation(1));
        // to queue emails do ->queue()

    }

}