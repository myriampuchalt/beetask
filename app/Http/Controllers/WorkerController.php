<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Job;
use Illuminate\Support\Facades\Auth;

class WorkerController extends Controller
{
	public function getWorker($id)
	{
		return User::find($id);
	}
}