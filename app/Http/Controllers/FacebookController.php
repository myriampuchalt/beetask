<?php

namespace app\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;

class FacebookController extends Controller
{
    /**
     * Redirect the user to the Facebook authentication page
     * @return \Illuminate\Http\Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook and process the callback
     * @return array
     */
    public function handleFacebookCallback()
    {
        $facebookUser = Socialite::driver('facebook')->user();
        $appUser = User::getUserByFacebookId($facebookUser->getId());

        if (!$appUser && $appUser = User::getUserByFacebookEmail($facebookUser->getEmail())) {
            $appUser->facebook_id = $facebookUser->getId();
            $appUser->save();
        } elseif (!$appUser) {
            return ['error' => 'User Not Registered'];
        }

        auth()->login($appUser, true);
        return redirect()->to('/account/profile');
    }

    public function logFacebookUser(Request $request)
    {
        $appUser = User::getUserByFacebookId($request->input('facebookId'));
        auth()->login($appUser, true);
        return 'true';
    }

    private function logFacebookUserIn($appUser)
    {
        auth()->login($appUser, true);
        //return redirect()->route('profile');
        return redirect()->to('/account/profile');
    }

    public function hasFacebookId($facebookId)
    {
        return User::getUserByFacebookId($facebookId) ? 'true' : 'false';
    }

}