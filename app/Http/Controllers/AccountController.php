<?php
/**
 * Created by PhpStorm.
 * User: Myriam
 * Date: 10/03/2018
 * Time: 13:20
 */

namespace App\Http\Controllers;

use App\Categories;
use App\Reviews;
use App\WorkerRates;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Address;
use App\Job;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadUserProfile()
    {
        $user = User::find(Auth::id());
        $workerRate = new WorkerRates();
        $workerCategories = $workerRate->findCategoriesByUserId(Auth::id());
        $workerRates = $workerRate->findByUserId(Auth::id());
        $categories = Categories::all();
        $address = Address::find($user['primary_address']);
        if ($address['locality']) {
            $address['town'] = $address['locality'];
        }
        return view('partials.accountProfile', [
            'user' => $user,
            'categories' => $categories,
            'workerCategories' => $workerCategories,
            'workerRates' => $workerRates,
            'address' => $address,
            'profilePicture' => $user['profile_picture'] ? $user['profile_picture'] : 'icon.png'
        ]);
    }

    public function updateUserProfile(Request $request)
    {
        $user = User::find(Auth::id());
        $user->name = $request->input('nickname');
        $user->phone_number = $request->input('phone_number');
        $user->mobile_number = $request->input('mobile_number');
        $user->save();

        $workerRate = new WorkerRates();
        $workerRate->deleteAllByUserId(Auth::id());

        $categories = Categories::findAll();
        foreach ($categories as $category) {
            $rate = $request->input('rateCategoryID_' . $category['id']);
            if ($rate) {
                $workerRate = new WorkerRates();
                $workerRate->worker_id = Auth::id();
                $workerRate->category_id = $category['id'];
                $workerRate->rate = $rate;
                $workerRate->save();
            }
        }

        $address = Address::find($user['primary_address']);

        if (!$address) {
            $address = new Address();
            $address->user_id = Auth::id();
        }

        $address->name = $request->input('name');
        $address->surname = $request->input('surname');
        $address->street_number = $request->input('street_number');
        $address->address = $request->input('address');
        $address->administrative_area_level_1 = $request->input('administrative_area_level_1');
        $address->postcode = $request->input('postcode');
        $address->country = $request->input('country');
        if ($request->input('town')) {
            $address->town = $request->input('town');
        } else if ($request->input('locality')) {
            $address->locality = $request->input('locality');
        }
        $address->save();
        $user->primary_address = $address->id;
        $user->save();
    }

    public function updateUserAddress(Request $request)
    {
        $user = User::find(Auth::id());
        $address = Address::find($user['primary_address']);

        if ($address) {
            $address->name = $request->name;
            $address->surname = $request->surname;
            $address->street_number = $request->street_number;
            $address->address = $request->address;
            $address->administrative_area_level_1 = $request->administrative_area_level_1;
            $address->administrative_area_level_2 = $request->administrative_area_level_2;
            $address->administrative_area_level_3 = $request->administrative_area_level_3;
            $address->locality = $request->locality;
            $address->town = $request->town;
            $address->postcode = $request->postcode;
            $address->country = $request->country;
//            $address->lat = $request->lat;
//            $address->lng = $request->lng;
            $address->save();
        } else {
            $address = $this->createUserAddress($request);
            $user->primary_address = $address->id;
            $user->save();
        }
    }

    public function createUserAddress(Request $request)
    {
        $address = Address::create([
            'user_id' => Auth::id(),
            'name' => $request->name,
            'surname' => $request->surname,
            'street_number' => $request->street_number,
            'address' => $request->address,
            'administrative_area_level_1' => $request->administrative_area_level_1,
            'administrative_area_level_2' => $request->administrative_area_level_2,
            'administrative_area_level_3' => $request->administrative_area_level_3,
            'locality' => $request->locality,
            'town' => $request->town,
            'postcode' => $request->postcode,
            'country' => $request->country,
//            'lat' => $request->lat,
//            'lng' => $request->lng,
        ]);

        return $address;
    }

    public function uploadProfilePicture(Request $request)
    {
        $validator = $this->imageValidator($request->toArray());

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $image = $request->file('image');
            $imageName = Auth::id() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/profile');
            $image->move($destinationPath, $imageName);
            $this->saveProfilePictureName($imageName);

            return back()->with('success', 'Image Upload successful');
        }
    }

    private function saveProfilePictureName($imageName)
    {
        $user = User::find(Auth::id());
        $user->profile_picture = $imageName;
        $user->save();
    }

    /**
     * Get a validator for incoming request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function imageValidator(array $data)
    {
        return Validator::make($data, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    }

    public function loadUserJobs()
    {
        $job = new Job();
        $jobs = $job->findByUserId(Auth::id());

        return view('partials.accountJobs', ['jobs' => $jobs]);
    }

    public function getUserJobs(Request $request)
    {
        $job = new Job();
        $jobs = $job->findByUserId(Auth::id(), null, $request->input('orderBy'));
        return view('partials.jobsContainer', ['jobs' => $jobs]);
    }

    public function loadUserReviews()
    {
        $reviews = Reviews::findByUserId(Auth::id());
        return view('partials.accountReviews', ['reviews' => $reviews]);
    }

    public function loadAddress(Request $request)
    {
        return Address::find($request->input['id']);
    }

    public function loadAccountTimeline()
    {
        return view('partials.accountTimeline');
    }

    public function loadTimeline(Request $request)
    {
        $date = $request->input('date');
        $userPosition = $request->input('position');
        $date = new Carbon($date);
        $job = new Job();
        $jobs = $job->findByUserId(Auth::id(), $date->day);
        $now = Carbon::now('Europe/Madrid');

        $jobsDistances = [];
        $nextJob = null;
        $currentTime = $now->toTimeString();

        $siteMessage = null;
        $userWillBeLate = false;

        if (count($jobs) > 1) {
            for ($i = 0; $i < count($jobs); $i++) {

                $jobTime = $jobs[$i]['start_time_object']->toTimeString();

                if (!$nextJob && $currentTime < $jobTime) {
                    $nextJob = $jobs[$i];
                    $jobs[$i]['next_job'] = true;
                }

                if ($i == count($jobs) - 1) {
                    continue;
                }

                $index = $i;
                $distanceMatrix = self::getDistanceMatrix($jobs[$i], $jobs[$index + 1]);

                $jobsDistances[$i]['dist'] = $distanceMatrix['rows'][0]['elements'][0]['distance']['text'];
                $jobsDistances[$i]['time'] = $distanceMatrix['rows'][0]['elements'][0]['duration']['text'];
            }
        } elseif (count($jobs) == 1) {
            $jobTime = $jobs[0]['start_time_object']->toTimeString();

            if ($currentTime < $jobTime) {
                $nextJob = $jobs[0];
                $jobs[0]['next_job'] = true;
            }
        }

        if ($nextJob) {
            $nextJobDueIn = $nextJob['start_time_object']->diffInMinutes($now);
            $nextJobDueIn = $nextJobDueIn - 60;
            $dueInText = ' minutes.';
            if ($nextJobDueIn > 60) {
                $nextJobDueIn = $nextJob['start_time_object']->diffInHours($now);
                $dueInText = ' hours.';
            }
            $siteMessage = 'Next job due in ' . $nextJobDueIn . $dueInText;

            if ($userPosition) {

                $distanceMatrix = self::getDistanceMatrix($userPosition['coords'], $nextJob);

                $timeToGetToNextJobInSeconds = $distanceMatrix['rows'][0]['elements'][0]['duration']['value'];
                $distanceToGetToNextJob = $distanceMatrix['rows'][0]['elements'][0]['distance']['text'];

                $siteMessage .= 'Distance to next job: ' . $distanceToGetToNextJob . '. ';

                $timeWhenCouldBeAtJob = $now->addSeconds($timeToGetToNextJobInSeconds);

                if ($timeWhenCouldBeAtJob->toDateString() <= $nextJob['start_time_object']->toDateString() &&
                    $timeWhenCouldBeAtJob->toTimeString() < $nextJob['start_time_object']->toTimeString()) {
                    $siteMessage .= 'You are still in time.';
                } else {
                    $userWillBeLate = true;
                    $siteMessage .= 'You will be late, please let the customer know.';
                }
            }
        }

        $date = $date->format('jS \o\f F, Y');
        return view('partials.timeline', [
            'jobs' => $jobs,
            'date' => $date,
            'jobDistances' => $jobsDistances,
            'siteMessage' => $siteMessage,
            'userWillBeLate' => $userWillBeLate
        ]);
    }

    private static function getDistanceMatrix($job1, $job2)
    {
        $lat1 = $job1['latitude'] ? $job1['latitude'] : $job1['lat'];
        $lat2 = $job2['lat'];
        $lng1 = $job1['longitude'] ? $job1['longitude'] : $job1['lng'];
        $lng2 = $job2['lng'];

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $lng1 . "&destinations=" . $lat2 . "," . $lng2 . "&key=AIzaSyBCW5Q349p75JSwOn7tbVpldecyRRp-qlU&mode=driving&language=en-GB";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response, true);
    }
}
