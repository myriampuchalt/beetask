<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use App\UserProfileComplete;
use App\WorkerRates;
use App\WorkerAccounts;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\RegistersUsers;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Address;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class WorkerSignUpController extends Controller
{
    use EntrustUserTrait; //This will enable the relation with Role and add the following methods roles(), hasRole($name), can($permission), and ability($roles, $permissions, $options) within your User model.

    /*
    |--------------------------------------------------------------------------
    | Worker Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new workers as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register/complete';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('guest');
//    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'mobile_number' => 'required|string|max:15',
            'postcode' => 'required|string|max:10'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'phone_number' => $data['mobile_number']
        ]);

        $role = Role::findOrFail(Role::ROLE_WORKER);
        $user->attachRole($role);

        $address = Address::create([
            'user_id' => $user->id,
            'name' => $data['name'],
            'postcode' => $data['postcode'],
        ]);

        $user->primary_address = $address->id;
        $user->save();
        return $user;
    }

    public function showWorkerRegistrationForm()
    {
        return view('auth.workerSignUp');
    }

    public function showCompleteSignUpPage()
    {
        $categories = Categories::findAll();
        $user = User::find(Auth::id());
        $address = Address::find($user->primary_address);
        return view('completeWorkerSignUp', [
            'categories' => $categories,
            'user' => $user,
            'address' => $address
        ]);
    }

    public function completeSignUp(Request $request)
    {
        $validator = $this->completeSignUpFormValidator($request->toArray());
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            // Save Profile Picture
            $image = $request->file('image');
            $imageName = Auth::id() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/profile');
            $image->move($destinationPath, $imageName);
            // Save worker rates for selected categories
            foreach ($request->input('category') as $categoryId) {
                WorkerRates::create([
                    'worker_id' => Auth::id(),
                    'category_id' => $categoryId,
                    'rate' => $request->input('worker_rate.' . $categoryId)
                ]);
            }
            // Save bank account details
            WorkerAccounts::create([
                'worker_id' => Auth::id(),
                'account_number' => $request->input('account_number'),
                'sort_code' => $request->input('sort_code')
            ]);
            // Save rest of Personal Data
            $user = User::find(Auth::id());
            $address = Address::find($user->primary_address);

            $address->name = $request->input('first_name');
            $address->surname = $request->input('surname');
            if ($request->input('company_name')) {
                $address->company_name = $request->input('company_name');
            }
            $address->address = $request->input('address_line');
            $address->postcode = $request->input('postcode');
            $address->town = $request->input('town');
            $address->save();

            $user->name = $request->input('first_name');
            $user->surname = $request->input('surname');
            $user->phone_number = $request->input('phone_number');
            if ($request->input('mobile_number')) {
                $user->mobile_number = $request->input('mobile_number');
            }
            $user->profile_picture = $imageName;
            $user->description = $request->input('worker_description');
            $user->save();

            // Mark worker profile as complete
            UserProfileComplete::create([
                'user_id' => Auth::id(),
                'profile_complete' => true
            ]);

            return redirect()->to('/account/profile');
        }
    }

    protected function completeSignUpFormValidator(array $data)
    {
        $messages = [
            'worker_rate.*.required' => 'Please set a rate when selecting a skill.',
            'category.required' => 'Please select at least one skill.'
        ];

        return Validator::make($data, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category' => 'required',
            'worker_rate.*' => 'required',
            'first_name' => 'required',
//            'surname' => 'required',
            'phone_number' => 'required|numeric',
//            'address_line' => 'required',
            'postcode' => 'required',
//            'town' => 'required',
            'account_number' => 'required|numeric',
            'sort_code' => 'required|numeric',
            'worker_description' => 'required|max:255'
        ], $messages);
    }
}
