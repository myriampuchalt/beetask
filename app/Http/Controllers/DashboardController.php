<?php

namespace App\Http\Controllers;

use App\Address;
use App\Job;
use App\Reviews;
use App\User;
use App\Categories;
use App\Worker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Stripe\Charge;
use App\Events\JobAssigned;
use JavaScript;
use Illuminate\Support\Facades\Mail;
use App\Mail\CustomerBookingConfirmation;
use App\Mail\WorkerBookingConfirmation;


class DashboardController extends Controller
{
    // saveAddress should be used as well in AccountController.
    // Maybe we should create a trait for functions that can be used in different controllers

    // Unused for now
//	public function savePrimaryAddress(Request $request)
//	{
//		$user = User::find(Auth::id());
//		$address = Address::find($user['primary_address']);
//
//		if ($address) {
//			$address->updateAddress($request);
//		} else {
//			$address = Address::createAddress($request->all());
//			$user->updatePrimaryAddress($address->id);
//		}
//
//		return $address;
//	}

    public function getWorkerReviews(Request $request)
    {
        $categoryId = $request->input('category');
        $workerId = $request->input('workerId');
        $worker = User::find($workerId);
        $reviews = Reviews::findByUserAndCategory($workerId, $categoryId);

        return view('partials.workersReviews', [
            'worker' => $worker,
            'reviews' => $reviews,
        ]);
    }

    public function createJob(Request $request)
    {
        // The Request is a Multidimensional Array
        // jobDetails => {0=>category, 1=>taskDescription, 2=>date, 3=>time, 4=>totalHours}
        // address => address fields

        $jobDetails = $request->input('jobDetails');
        $jobAddress = $request->input('address');

        $address = new Address();
        $address->addJobAddress($jobAddress);

        $job = new Job();
        $job->addJobDetails($jobDetails, $address);

        return ['jobId' => $job->getKey()];
    }

    public function getAvailableWorkers(Request $request)
    {
        // The Request is a Multidimensional Array
        // jobDetails => {0=>category, 1=>taskDescription, 2=>date, 3=>time, 4=>totalHours}

        $jobDetails = $request->input('jobDetails');
        $category = $jobDetails[0]['value'];
        $startDate = $jobDetails[2]['value'];
        $startTime = $jobDetails[3]['value'];
        $jobDuration = $jobDetails[4]['value'];

        $jobStart = new Carbon($startDate . ' ' . $startTime);

        // We return all available workers for those request filters
        $workers = Job::getAvailableWorkers($jobStart, $jobDuration, $category);

        return view('partials.workersContainer', [
            'workers' => $workers
        ]);
    }

    public function showCustomerJobForm()
    {
        $categories = Categories::all();

        if (Auth::check()) {
            JavaScript::put([
                'userId' => Auth::id()
            ]);
        }

        return view('dashboard', [
            'categories' => $categories
        ]);
    }

    public function allocateUser(Request $request)
    {
        $job = Job::find($request->input('jobId'));
        $job->allocateUser(Auth::id());
    }

    public function getCheckoutData(Request $request)
    {
        $user = User::find(Auth::id());
        $job = Job::find($request->input('jobId'));
        $category = Categories::findById($job->category_id);

        $js = new Carbon($job->start_time);
        $je = new Carbon($job->end_time);

        $jobStart = $js->format('d-m-Y H:i:s');

        $job['category_name'] = $category->ident;
        $job['duration'] = $je->diffInHours($js);
        $job['job_start'] = $jobStart;

        $address = Address::find($job->address_id);
        $renderedAddress = $address ? $address->render() : '';
        $workerId = $request->input('workerId');
        $worker = User::find($workerId);
        $workerRate = Worker::getWorkerRate($workerId, $job->category_id);
        $job['total_amount'] = $workerRate * $job['duration'];

        $orderDetails = [
            'user' => $user ? $user : null,
            'job' => $job ? $job : null,
            'address' => $renderedAddress,
            'worker' => $worker ? $worker : null
        ];

        return view('orderDetails', $orderDetails);
    }

    public function showOrderDetails($user, $job, $address, $worker)
    {
        return view('orderDetails', [
            'user' => $user,
            'job' => $job,
            'address' => $address,
            'worker' => $worker
        ]);
    }

    public function bookJob(Request $request)
    {
        // Not sure what to do with billingDetails yet
        Stripe::setApiKey("sk_test_eX8HHxD7kqSP8FD5ysXbV3lO");

        $charge = Charge::create([
            'amount' => $request->input('billingAmount') * 100, //amount needs to be in cents
            'currency' => 'gbp',
            //'source' => $request->input('stripeToken'),
            'source' => 'tok_visa',
            'statement_descriptor' => 'beeWork Charge',
            'description' => 'beeWork Test Charges'
        ]);

        if ($charge['paid'] == true && $charge['captured'] == true) {
            $job = Job::find($request->input('jobId'));
            $worker = User::find($request->input('workerId'));
            $job->status = Job::STATUS_ACTIVE;
            $job->worker_id = $worker->id;
            $job->save();

            Mail::to(Auth::user())->send(new customerBookingConfirmation($job->id));
            Mail::to($worker)->send(new workerBookingConfirmation($job->id));
            broadcast(new JobAssigned($job, $worker));

            return view('partials.orderConfirmation', [
                'jobId' => $job->id
            ]);
        }
    }

    public function getUserRole()
    {
        return Auth::check() ? Auth::user()->roles()->first()->name : 'false';
    }
}
