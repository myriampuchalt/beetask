<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    //use Socialite;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Check user's role and redirect user based on their role
     * @return
     */
    public function authenticated()
    {
        if(Auth::user()->hasRole('worker'))
        {
            $profileComplete = DB::table('user_profile_complete')->where('user_id', Auth::id())->first();
            if ($profileComplete) {
                return redirect()->to('/account/calendar');
            }
            return redirect()->to('/register/complete');
        }

        if(Auth::user()->hasRole('customer'))
        {
            return redirect('dashboard');
        }
        return redirect('/');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return array
     */
    public function handleFacebookCallback()
    {
        $facebookUser = Socialite::driver('facebook')->user();
        $appUser = User::getUserByFacebookId($facebookUser->getId());

        if (!$appUser && $appUser = User::getUserByFacebookEmail($facebookUser->getEmail())) {
            $appUser->facebook_id = $facebookUser->getId();
            $appUser->save();
        } elseif (!$appUser) {
            return ['error' => 'User Not Registered'];
        }

        auth()->login($appUser, true);
        return redirect()->to('/account/profile');
    }
}
