<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerHolidays extends Model
{
    protected $table = 'worker_holidays';
    protected $primaryKey = "id";

    public function user()
    {
        $this->belongsTo('User', 'user_id');
    }
}
