<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerRates extends Model
{
	protected $table = 'worker_rates';
	protected $primaryKey = 'id';

	protected $fillable = ['worker_id', 'category_id', 'rate'];

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function findCategoriesByUserId($id)
    {
        $workerRates = WorkerRates::where('worker_id', $id)->get();
        $categories = [];

        foreach($workerRates as $rate) {
            $categories[] = $rate->category_id;
        }
        return $categories;
    }

    public function findByUserId($id)
    {
        $workerRates = WorkerRates::where('worker_id', $id)->get();
        $rates = [];

        foreach($workerRates as $rate) {
            $rates[$rate->category_id] = $rate->rate;
        }
        return $rates;
    }

    public function deleteAllByUserId($id)
    {
        WorkerRates::where('worker_id', $id)->delete();
    }



}
