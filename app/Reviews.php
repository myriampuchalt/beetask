<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
	protected $table = 'reviews';
	protected $primaryKey = "id";

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public static function findByUserAndCategory($userId, $categoryId)
	{
		$reviews = Reviews::where('user_id', $userId)
            ->where('category_id', $categoryId)
            ->simplePaginate(5);

		foreach ($reviews as $index => $review) {
			$reviewer = User::find($review['reviewer_id']);
			$reviews[$index]['reviewer_name'] = $reviewer->name;
		}
		return $reviews;
	}

	public static function findByUserId($userId)
	{
		$reviews = Reviews::where('user_id', $userId)->get();

		foreach ($reviews as $index => $review) {
			$reviewer = User::find($review['reviewer_id']);
			$category = Categories::findById($review['category_id']);
			$reviews[$index]['reviewer_name'] = $reviewer->name;
			$reviews[$index]['category_name'] = $category->ident;
		}
		return $reviews;
	}
}
