<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User
 * @package App
 *
 * @property int primary_address
 *
 */
class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait; //This will enable the relation with Role and add the following methods roles(), hasRole($name), can($permission), and ability($roles, $permissions, $options) within your User model.

	protected $table = 'users';
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'phone_number', 'mobile_number', 'primary_address', 'password', 'facebook_id', 'profile_picture', 'rating'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public static function find($id)
	{
		return self::where('id', $id)->first();
	}

	// Unused for now

//	public function updatePrimaryAddress($addressId)
//	{
//		$this->primary_address = $addressId;
//		$this->save();
//	}

    public static function getUserByFacebookId($facebookId)
    {
        return self::where('facebook_id', $facebookId)->first();
    }

    public static function getUserByFacebookEmail($facebookEmail)
    {
        return self::whereEmail($facebookEmail)->first();
    }
}
