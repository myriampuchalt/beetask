<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Worker
{
	public static function getWorkerRate($workerId, $categoryId)
    {
        return DB::table('worker_rates')
                ->select('rate')
                ->where('worker_id', '=', $workerId)
                ->where('category_id', '=', $categoryId)
                ->first()->rate;
    }

}