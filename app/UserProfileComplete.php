<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class UserProfileComplete extends Model
{
    protected $table = 'user_profile_complete';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'profile_complete'];
}