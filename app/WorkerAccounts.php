<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class WorkerAccounts extends Model
{
    protected $table = 'worker_accounts';
    protected $primaryKey = 'id';

    protected $fillable = ['worker_id', 'account_number', 'sort_code'];

}