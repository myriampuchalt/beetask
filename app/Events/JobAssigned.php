<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow; //This is just used in dev to avoid queue the events and broadcast directly
use Carbon\Carbon;

use App\Job;
use App\User;

class JobAssigned implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $job;
    protected $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Job $job, User $worker)
    {
        $this->job = $job;
        $this->user = $worker;
        // Write the logic to send an email out & show popup on Workers profile
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('worker.' . $this->user->id);
    }

    // Used to customise the broadcast name. By default it is the class names'
//    public function broadcastAs()
//    {
//        return 'job-assigned';
//    }

    // Used to customise the Body of the broadcast if we want to hide/not include certain information from the passed object.
    public function broadcastWith()
    {
        $startTimeObject = new Carbon($this->job->start_time);
        $endTimeObject = new Carbon($this->job->end_time);

        return [
            'label' => $this->job->label,
            'start_date' => $startTimeObject->format('d M Y'),
            'start_time' => $startTimeObject->format('h:i a'),
            'duration' => $endTimeObject->diffInHours($startTimeObject)
        ];
    }

    /**
     * Determine if this event should broadcast.
     *
     * @return bool
     */
//    public function broadcastWhen()
//    {
//        return $this->value > 100;
//    }
}
