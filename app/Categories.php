<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	protected $table = 'categories';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 * @var array
	 */
	protected $fillable = ['ident'];

	public static function findAll()
	{
		return Categories::all();
	}

	public static function findById($categoryId)
	{
		return Categories::where('id', $categoryId)->first();
	}
}
