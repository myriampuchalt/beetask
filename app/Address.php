<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Address
 * @package App
 *
 * @property int user_id
 * @property string street_number
 * @property string address
 * @property string postcode
 * @property string town
 * @property string locality
 * @property string administrative_area_level_1
 * @property string administrative_area_level_2
 * @property string administrative_area_level_3
 * @property decimal lat
 * @property decimal lng
 * @property string country
 *
 */
class Address extends Model
{
	protected $table = 'addresses';
	protected $primaryKey = 'id';

	protected $fillable = [
		'user_id',
		'name',
		'surname',
		'administrative_area_level_1',
		'administrative_area_level_2',
		'administrative_area_level_3',
		'locality',
		'postcode',
		'town',
		'lat',
		'lng',
		'address',
		'country',
        'street_number'
		];

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public static function find($id)
	{
		return self::where('id', $id)->first();
	}

	public function addJobAddress($jobAddress)
    {
        // address: might contain any of the following if found when entered
        // id, lat, lng, street_number, administrative_area_level_1, administrative_area_level_2, administrative_area_level_3,
        // locality, postcode, town, address, country

        if (Auth::check()) {
            $this->user_id = Auth::id();
        }

        foreach($jobAddress as $property => $value) {
            if ($property == 'id') {
                continue;
            }
            $this->$property = $value;
        }
        $this->save();
        return $this;
    }

    // Unused for now

//	public function updateAddress($request)
//	{
//		$this->street_number = $request->input('street_number');
//		$this->address = $request->input('address');
//		$this->postcode = $request->input('postcode');
//		$this->town = $request->input('town');
//		$this->locality = $request->input('locality');
//		$this->administrative_area_level_1 = $request->input('administrative_area_level_1');
//		$this->administrative_area_level_2 = $request->input('administrative_area_level_2');
//		$this->administrative_area_level_3 = $request->input('administrative_area_level_3');
//		$this->lat = $request->input('lat');
//		$this->lng = $request->input('lng');
//		$this->country = $request->input('country');
//		$this->save();
//	}

	public function render()
	{
		$formattedAddress = $this->street_number . ' ' . $this->address . '<br />';
		$formattedAddress .= $this->postcode . ', ' . '<br />';
		$formattedAddress .= $this->town ? $this->town : $this->administrative_area_level_2 . ', ' . $this->country;

		return $formattedAddress;
	}
}
