<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Job extends Model
{
    protected $table = 'job';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'category_id', 'label', 'start_time', 'end_time', 'address_id', 'status', 'description', 'locale'];

    const STATUS_PENDING = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_COMPLETED = 3;

    const EN_GB_LOCALE = 'en-gb';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public static function find($id)
    {
        return self::where('id', $id)->first();
    }

    public function findByUserId($userId, $day = null, $orderBy = 'start_time', $order = 'desc')
    {

        $jobs = $this;

        if (Auth::user()->hasRole('worker')) {
            $jobs = $jobs->where('worker_id', $userId);
        }
        if (Auth::user()->hasRole('customer')) {
            $jobs = $jobs->where('user_id', $userId);
        }

        if ($day) {
            $jobs = $jobs->whereDay('start_time', $day);
        }

        $validStatuses = [Job::STATUS_ACTIVE, Job::STATUS_COMPLETED];

        $jobs = $jobs->whereIn('status', $validStatuses)->orderBy($orderBy, $order)->get();

        return $jobs ? $this->addNeededPropertiesToJobs($jobs) : $jobs;
    }

    private function addNeededPropertiesToJobs($jobs)
    {
        foreach ($jobs as $index => $job) {
            $user = User::find($job->user_id);
            $worker = User::find($job->worker_id);
            $category = Categories::findById($job->category_id);
            $address = Address::find($job->address_id);
            $startTime = new Carbon($job->start_time);
            $endTime = new Carbon($job->end_time);
            $jobs[$index]['user_name'] = $user->name;
            $jobs[$index]['worker_name'] = $worker->name;
            $jobs[$index]['category_name'] = $category->name;
            $jobs[$index]['duration'] = $endTime->diffInHours($startTime);
            $jobs[$index]['date'] = $startTime->format('d M Y');
            $jobs[$index]['start_time'] = $startTime->format('h:ia');
            $jobs[$index]['start_time_object'] = $startTime;
            $jobs[$index]['end_time'] = $endTime->format('h:ia');
            $jobs[$index]['date'] = $startTime->format('d M Y');
            $jobs[$index]['postcode'] = $address->postcode;
            $jobs[$index]['lat'] = $address->lat;
            $jobs[$index]['lng'] = $address->lng;
        }
        return $jobs;
    }

    public function addJobDetails($jobDetails, Address $address)
    {
        $categoryId = $jobDetails[0]['value'];
        $category = Categories::find($categoryId);

        $taskDescription = $jobDetails[1]['value'];
        $startDate = $jobDetails[2]['value'];
        $startTime = $jobDetails[3]['value'];

        $startDateTime = new Carbon($startDate . ' ' . $startTime);

        $this->category_id = $categoryId;
        $this->label = $category->name . ' in ' . $address->address;
        $this->start_time = $startDateTime;
        $this->end_time = $startDateTime->copy()->addHours($jobDetails[4]['value']);
        $this->address_id = $address->id;
        $this->status = self::STATUS_PENDING;
        $this->description = $taskDescription;
        // We won't be using locales for now, so we'll set the locale to 'en-gb' by default
        $this->locale = self::EN_GB_LOCALE;

        if (Auth::check()) {
            $this->user_id = $address->user_id;
        }

        $this->save();
        return $this;
    }

    public function allocateUser($userId)
    {
        $this->user_id = $userId;
        $this->save();
    }

    public static function getAvailableWorkers($jobStart, $jobDuration, $category)
    {
        $jobEnd = $jobStart->copy()->addHours($jobDuration);

        $workersWithAvailability = DB::table('users')
            ->select('users.id')
            ->join('worker_availability', 'users.id', '=', 'worker_availability.user_id')
            ->join('worker_rates', 'worker_availability.user_id', '=', 'worker_rates.worker_id')
            ->where('worker_rates.category_id', $category);

        if ($jobStart->subHour()->toTimeString() < '13:00:00' || $jobEnd->addHour()->toTimeString() <= '13:00:00') {
            $workersWithAvailability->where('morning', 1);
        }
        if (
            ($jobStart->subHour()->toTimeString() >= '13:00:00' && $jobStart->subHour()->toTimeString() < '18:00:00') ||
            ($jobEnd->addHour()->toTimeString() > '13:00:00' && $jobEnd->addHour()->toTimeString() <= '18:00:00')
        ) {
            $workersWithAvailability->where('afternoon', 1);
        }
        if ($jobStart->subHour()->toTimeString() >= '18:00:00' || $jobEnd->addHour()->toTimeString() > '18:00:00') {

            $workersWithAvailability->where('evening', 1);
        }

        $workersWithAvailability->distinct();

        $idsWorkersWithAvailability = $workersWithAvailability->pluck('id');
        $collectionWorkersWithAvailability = collect($idsWorkersWithAvailability);

        $bookedWorkers = DB::table('users')
            ->select('users.id')
            ->join('job', 'job.worker_id', '=', 'users.id')
            ->whereIN('job.worker_id', $idsWorkersWithAvailability)
            ->whereRaw('DATE(job.start_time) = "' . $jobStart->toDateString() . '"')
            ->where(function ($query) use ($jobStart, $jobEnd) {
                $query->whereRaw('job.start_time BETWEEN \'' . $jobStart->subHour()->toDateTimeString()
                    . '\' AND \'' . $jobEnd->addHour()->toDateTimeString() . '\'')
                    ->orWhereRaw('job.end_time BETWEEN \'' . $jobStart->subHour()->toDateTimeString()
                        . '\' AND \'' . $jobEnd->addHour()->toTimeString() . '\'');
            });

        $collectionBookedWorkers = collect($bookedWorkers->distinct()->pluck('id'));

        $availableWorkerIds = $collectionWorkersWithAvailability->diff($collectionBookedWorkers);

        return DB::table('users')
            ->whereIn('users.id', $availableWorkerIds)
            ->join('worker_rates', 'users.id', '=', 'worker_rates.worker_id')
            ->where('worker_rates.category_id', $category)
            ->simplePaginate(8);

//        This query is for testing purposes
//        return  DB::table('users')
//            ->join('worker_rates', 'users.id', '=', 'worker_rates.worker_id')
//            ->simplePaginate(8);
    }
}
