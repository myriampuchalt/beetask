<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerAvailability extends Model
{
    protected $table = 'worker_availability';
    protected $primaryKey = 'id';

	protected $fillable = ['user_id', 'day_of_week', 'morning', 'afternoon', 'evening', 'date'];

    public function user()
    {
        $this->belongsTo('User', 'user_id');
    }

    public static function findByDayOfWeek($userId, $dayOfWeek)
    {
        return WorkerAvailability::where('user_id', $userId)->where('day_of_week', $dayOfWeek)->first();
    }
}
