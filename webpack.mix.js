let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/homepage.js', 'public/js')
    .js('resources/assets/js/dashboard.js', 'public/js')
    .js('resources/assets/js/accountProfile.js', 'public/js')
    .js('resources/assets/js/accountCalendar.js', 'public/js')
    .js('resources/assets/js/accountJobs.js', 'public/js')
    .js('resources/assets/js/accountTimeline', 'public/js')
    .js('resources/assets/js/checkout.js', 'public/js')
    .js('resources/assets/js/workerAccount.js', 'public/js')
    .js('node_modules/jquery/src/jquery.js', 'public/js')
    .js('resources/assets/js/signUp.js', 'public/js')
    .copy('node_modules/underscore/underscore.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/signUp.scss', 'public/css')
    .sass('resources/assets/sass/dashboard.scss', 'public/css')
    .sass('resources/assets/sass/account.scss', 'public/css')
    .sass('resources/assets/sass/accountTimeline.scss', 'public/css')
    .sass('resources/assets/sass/header.scss', 'public/css')
    .sass('resources/assets/sass/homepage.scss', 'public/css')
    .copy('node_modules/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js', 'public/js')
    .less('node_modules/eonasdan-bootstrap-datetimepicker/src/less/bootstrap-datetimepicker-build.less', 'public/css')
    .copy('node_modules/fullcalendar/dist/fullcalendar.js', 'public/js')
    .copy('node_modules/fullcalendar-scheduler/dist/scheduler.js', 'public/js')
    .copy('node_modules/fullcalendar/dist/fullcalendar.css', 'public/css')
    .copy('node_modules/moment/moment.js', 'public/js')
    .copy('node_modules/moment/locale/en-gb.js', 'public/js')
    .copy('node_modules/bootstrap/dist/js/bootstrap.js', 'public/js')
    .copy('resources/assets/js/notify.js', 'public/js')
    .copyDirectory('node_modules/jquery-ui/themes/base', 'public/css')
    // sourceMaps() will provide extra debugging information to your browser's developer tools when using compiled assets
    .sourceMaps();

// example plain CSS

// mix.styles([
//     'public/css/vendor/normalize.css',
//     'public/css/vendor/videojs.css'
// ], 'public/css/all.css');
