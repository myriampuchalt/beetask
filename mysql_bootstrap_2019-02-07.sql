# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.10.10 (MySQL 5.7.22-0ubuntu18.04.1)
# Database: homestead
# Generation Time: 2019-02-07 21:49:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

# Dump of table categories
# ------------------------------------------------------------

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `ident`, `created_at`, `updated_at`)
VALUES
	(1,'Babysitting','babysitting',NOW(),NOW()),
	(3,'Delivery','delivery',NOW(),NOW()),
	(4,'Cleaning','cleaning',NOW(),NOW()),
	(5,'Dogsitting','dogsitting',NOW(),NOW()),
	(6,'Furniture Assembly','assembly',NOW(),NOW()),
	(7,'Gardening','gardening',NOW(),NOW()),
	(8,'Handyman','handyman',NOW(),NOW()),
	(9,'Driving','driving',NOW(),NOW()),
	(10,'Computing & IT ','computing',NOW(),NOW()),
	(11,'Cooking','cooking',NOW(),NOW()),
	(12,'Photography','photography',NOW(),NOW());

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

# Dump of table roles
# ------------------------------------------------------------

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'admin','Admin',NULL,NOW(),NOW()),
	(2,'customer','Customer',NULL,NOW(),NOW()),
	(3,'worker','Worker',NULL,NOW(),NOW());

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
