<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// To enable CSRF in any route do:
//$router->post('url', ['middleware' => 'csrf', function() {
//    ...
//}]);

// The following line loads the routes from Illuminate/Routing/Router.php
Auth::routes();

Route::get('userRole', 'DashboardController@getUserRole')->name('userRole');

// Customer Routes -> dashboard (post a job)
Route::prefix('dashboard')->group(function () {
    Route::get('/', 'DashboardController@showCustomerJobForm')->name('dashboard');
	Route::get('workers', 'DashboardController@getAvailableWorkers')->name('workers');
	Route::get('reviews', 'DashboardController@getWorkerReviews')->name('workerReviews');
	Route::get('/checkout', 'DashboardController@getCheckoutData')->name('checkoutData');
	Route::post('/checkout', 'DashboardController@bookJob')->name('bookJob');
//    Route::post('address', 'DashboardController@savePrimaryAddress')->name('primaryAddress');
    Route::post('allocateUser', 'DashboardController@allocateUser')->name('allocateUser');
    Route::post('createJob', 'DashboardController@createJob')->name('createJob');

    Route::prefix('worker')->group(function () {
	    Route::get('/{id}', 'WorkerController@getWorker')->name('worker');
    });

    Route::post('/orderDetails', 'DashboardController@showOrderDetails')->name('orderDetails');
});

// Dashboard & Account Views

Route::view('/', 'homepage');
Route::get('/', function () {
    $categories = \App\Categories::findAll();
    return view('homepage', ['categories' => $categories]);
});
Route::view('terms', 'terms')->name('terms');
Route::view('privacy', 'privacy')->name('privacy');


// Account Views & Actions
Route::prefix('account')->group(function () {

    // Routes that don't include data stored in our DB and just need to load a view
    Route::view('/', 'account')->name('account');

    // Routes that require Data to load a view -> we use the controller to do so
    Route::get('weekly-availability', 'CalendarController@retrieveWorkerWeeklyAvailability')->name('availability');

    Route::prefix('profile')->group(function () {
		Route::get('/', 'AccountController@loadUserProfile')->name('profile');
		Route::post('/', 'AccountController@updateUserProfile')->name('profile');
		Route::post('/image', 'AccountController@uploadProfilePicture')->name('image');
        Route::get('address', 'AccountController@loadAddress')->name('address');
        Route::post('address', 'AccountController@updateUserAddress')->name('address');
	});

    Route::prefix('jobs')->group(function () {
        Route::get('/', 'AccountController@loadUserJobs')->name('jobs');
        Route::get('/filtered', 'AccountController@getUserJobs')->name('filteredJobs');
    });

	Route::prefix('calendar')->group(function () {
	    Route::get('/', 'CalendarController@loadCalendarView')->name('calendar');
        Route::post('/', 'CalendarController@processAvailability')->name('calendar');
        Route::get('events', 'CalendarController@retrieveCalendarEvents')->name('calendarEvents');
        Route::get('availability', 'CalendarController@retrieveWorkerAvailabilityForCalendar')->name('calendarEvents');
    });

	Route::prefix('timeline')->group(function () {
        Route::get('/', 'AccountController@loadAccountTimeline')->name('accountTimeline');
        Route::get('/content', 'AccountController@loadTimeline')->name('timeline');
    });
});

// Worker Registration
Route::prefix('register')->group(function () {
    Route::get('', 'WorkerSignUpController@showWorkerRegistrationForm')->name('workerSignUp')->middleware('guest');;
    Route::post('', 'WorkerSignUpController@register')->middleware('guest');;
    Route::get('/complete', 'WorkerSignUpController@showCompleteSignUpPage')->middleware('role:worker');
    Route::post('/complete', 'WorkerSignUpController@completeSignUp')->middleware('role:worker')->name('completeSignUp');
});

// User Registration
Route::post('userRegister', 'Auth\RegisterController@register')->name('userRegister');


// Send Emails
Route::post('/send', 'JobsController@sendBookingConfirmationEmail')->name('send');

Route::prefix('facebook')->group(function () {
    Route::get('callback', 'FacebookController@handleFacebookCallback');
    Route::get('redirect', 'FacebookController@redirectToFacebook')->name('facebookRedirect');
    Route::get('check/{facebookId}', 'FacebookController@hasFacebookId')->name('hasFacebookId');
    Route::post('login', 'FacebookController@logFacebookUser')->name('loginFacebookUser');
});

// Sandbox Testing Controller
Route::resource('s', 'SandboxController');