<?php

use Illuminate\Database\Seeder;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class, 2)
            ->create()
            ->each(function ($u) {
                $role = Role::findOrFail(Role::ROLE_WORKER);
                $u->attachRole($role);
            });
    }
}
