<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
        	$table->integer('user_id')->after('id')->unsigned();
            $table->string('administrative_area_level_1')->after('first_line')->nullable();
            $table->string('administrative_area_level_2')->after('administrative_area_level_1')->nullable();
            $table->string('administrative_area_level_3')->after('administrative_area_level_2')->nullable();
            $table->string('locality')->after('administrative_area_level_3')->nullable();
            $table->string('town')->after('locality')->nullable();
            $table->decimal('lat', 8, 6)->after('country')->nullable();
            $table->decimal('lng', 8, 6)->after('lat')->nullable();
	        $table->renameColumn('first_line', 'address');
	        $table->dropColumn('second_line');

	        $table->foreign('user_id')->references('id')->on('users')
		        ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
        });
    }
}
